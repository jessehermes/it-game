<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpiTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpi_team', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('team_id')->unsigned();
            $table->integer('kpi_id')->unsigned();
            $table->primary(array('team_id', 'kpi_id'));
            $table->integer('investment');

            $table->foreign('team_id')->references('id')->on('teams');
            $table->foreign('kpi_id')->references('id')->on('kpis')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpi_team');
    }
}
