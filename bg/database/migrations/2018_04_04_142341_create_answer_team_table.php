<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_team', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('game_session_id')->unsigned();
            $table->integer('question_id')->unsigned();
            $table->integer('team_id')->unsigned();
            $table->integer('answer_id')->unsigned();
            $table->primary(array('game_session_id', 'question_id', 'team_id'));

            $table->foreign('game_session_id')->references('game_session_id')->on('game_session_question');
            $table->foreign('question_id')->references('question_id')->on('game_session_question')->onDelete('cascade');
            $table->foreign('team_id')->references('id')->on('teams');
            $table->foreign('answer_id')->references('id')->on('answers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_team');
    }
}
