<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerKpiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_kpi', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('answer_id')->unsigned();
            $table->integer('kpi_id')->unsigned();
            $table->primary(array('answer_id', 'kpi_id'));
            $table->float('effect', 3, 2)->default(1.00);
            $table->string('explanation', 1000)->nullable(true);

            $table->foreign('answer_id')->references('id')->on('answers')->onDelete('cascade');
            $table->foreign('kpi_id')->references('id')->on('kpis')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_kpi');
    }
}
