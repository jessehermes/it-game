<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameSessionQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_session_question', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('game_session_id')->unsigned();
            $table->integer('question_id')->unsigned();
            $table->primary(array('game_session_id', 'question_id'));

            $table->foreign('game_session_id')->references('id')->on('game_sessions');
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_session_question');
    }
}
