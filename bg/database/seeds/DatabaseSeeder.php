<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(AnswersTableSeeder::class);
        $this->call(KpisTableSeeder::class);
        $this->call(AnswerKpiTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(FinesTableSeeder::class);
    }
}
