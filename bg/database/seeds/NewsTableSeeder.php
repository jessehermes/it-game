<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            'question_id' => 3,
            'title' => "Big data geeft banken inzicht in klantbehoeften",
            'news' => "Banken maken slim gebruik van een klein deel van de mogelijkheden die big data bieden. 70 procent van de IT-managers denkt dat de inzet van big data cruciaal is voor succes in de toekomst. 53 procent verwacht concurrentie van startups die in grote mate leunen op big data. 65 procent van de bedrijven denkt dat niet tijdig inbedden de concurrentiepositie schaadt. Marketing draait meer en meer op het slim combineren van verschillende datastromen en -bronnen. Organisaties die hiermee aan de slag gaan kunnen vrij nauwkeurig het optimale moment bepalen om contact te zoeken en een aanbieding te doen. Want als we prijzen kunnen voorspellen, waarom klantbehoeften dan niet?",
        ]);
        DB::table('news')->insert([
            'question_id' => 6,
            'title' => "Banken stappen actief in crowdfunding",
            'news' => "Midden- en kleinbedrijven krijgen sinds de crisis minder gemakkelijk een lening bij een bank. Dat komt onder meer door het aanscherpen van de regels voor banken. Bedrijven wijken dus noodgedwongen uit naar alternatieve financieringsvormen. Daarom focussen banken zich steeds vaker op een dienstverlenende en verbindende rol. Crowdfunding is daar een voorbeeld van.",
        ]);
        DB::table('news')->insert([
            'question_id' => 8,
            'title' => "Groene investeringen verliesgevend",
            'news' => "Duitse windenergie is niet rendabel. Een onderzoek naar meer dan 1150 financiële jaarverslagen van 175 windparken in de periode van 2000 tot 2011 heeft aangetoond dat de meeste windparken alle zeilen moeten bijzetten om zelfs maar break-even te komen. Het onderzoek werd verricht door Werner Daldorf, voorzitter van het Bundesverbandes WindEnergie. De helft van alle onshore windparken draait zo slecht dat investeerders blij mogen zijn als ze over 20 jaar hun geld met inflatieverlies terug kunnen zien. 37% van de jaarverslagen liet een negatieve cash-flow zien. De inkomsten van het windpark waren lager dan de rente- en aflossingskosten. Hier heeft de ‘groene’ industrie wel vaker last van, werken met aannames en rammelende klimaatmodellen. Het onderzoek werd door de groen-linkse krant TAZ wereldkundig gemaakt.",
        ]);
        DB::table('news')->insert([
            'question_id' => 9,
            'title' => "Blockchain is het topje van de ijsberg",
            'news' => "Sinds kort heeft de bankensector kennis gemaakt met blockchain. Het gebruik van deze techniek voor digitaal betalingsverkeer wordt gezien als het topje van de ijsberg. Sommigen noemen de blockchain de volgende grote digitale revolutie sinds de komst van het internet. Echter, uiteindelijk blijft blockchain nog veel bij experimenteren waarbij nog niets concreet is.",
        ]);
        DB::table('news')->insert([
            'question_id' => 10,
            'title' => "Zware boetes voor bank in overtreding",
            'news' => "De Europese Commissie wil dat toezichthouders de bevoegdheid krijgen om zware boetes op te leggen aan banken die de regelgeving overtreden. Dit blijkt uit voorstellen die de commissie woensdag heeft gepresenteerd. Met de voorstellen geeft Brussel invulling aan de invoering van het Basel III-akkoord. In dit akkoord, dat van vorig jaar dateert, zijn onder meer afspraken gemaakt over de kapitaaleisen die aan banken worden gesteld. De kapitaalvereisten voor banken worden in de nabije toekomst verscherpt. Dit betekent dat de dekkingsgraad van het kapitaal van de banken naar 120% gaat. Wanneer deze nieuwe regels ingaan is onbekend, maar indien de banken niet aan deze vereisten kunnen voldoen zal dit een boete als gevolg hebben.",
        ]);
    }
}
