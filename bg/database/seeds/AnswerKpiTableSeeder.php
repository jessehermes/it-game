<?php

use Illuminate\Database\Seeder;

class AnswerKpiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answer_kpi')->insert([
            'answer_id' => 1,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 1,
            'kpi_id' => 2,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 1,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 1,
            'kpi_id' => 4,
            'effect' => 0.96,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 2,
            'kpi_id' => 1,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 2,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 2,
            'kpi_id' => 3,
            'effect' => 0.92,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 2,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 3,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 3,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 3,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 3,
            'kpi_id' => 4,
            'effect' => 1.08,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 4,
            'kpi_id' => 1,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 4,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 4,
            'kpi_id' => 3,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 4,
            'kpi_id' => 4,
            'effect' => 1.08,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 5,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 5,
            'kpi_id' => 2,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 5,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 5,
            'kpi_id' => 4,
            'effect' => 0.96,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 6,
            'kpi_id' => 1,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 6,
            'kpi_id' => 2,
            'effect' => 0.92,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 6,
            'kpi_id' => 3,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 6,
            'kpi_id' => 4,
            'effect' => 0.92,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 7,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 7,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 7,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 7,
            'kpi_id' => 4,
            'effect' => 1.08,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 8,
            'kpi_id' => 1,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 8,
            'kpi_id' => 2,
            'effect' => 0.92,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 8,
            'kpi_id' => 3,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 8,
            'kpi_id' => 4,
            'effect' => 0.92,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 9,
            'kpi_id' => 1,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 9,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 9,
            'kpi_id' => 3,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 9,
            'kpi_id' => 4,
            'effect' => 0.96,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 10,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 10,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 10,
            'kpi_id' => 3,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 10,
            'kpi_id' => 4,
            'effect' => 1.00,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 11,
            'kpi_id' => 1,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 11,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 11,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 11,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 12,
            'kpi_id' => 1,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 12,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 12,
            'kpi_id' => 3,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 12,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 13,
            'kpi_id' => 1,
            'effect' => 0.92,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 13,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 13,
            'kpi_id' => 3,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 13,
            'kpi_id' => 4,
            'effect' => 1.00,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 14,
            'kpi_id' => 1,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 14,
            'kpi_id' => 2,
            'effect' => 0.92,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 14,
            'kpi_id' => 3,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 14,
            'kpi_id' => 4,
            'effect' => 0.96,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 15,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 15,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 15,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 15,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 16,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 16,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 16,
            'kpi_id' => 3,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 16,
            'kpi_id' => 4,
            'effect' => 1.00,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 17,
            'kpi_id' => 1,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 17,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 17,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 17,
            'kpi_id' => 4,
            'effect' => 1.08,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 18,
            'kpi_id' => 1,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 18,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 18,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 18,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 19,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 19,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 19,
            'kpi_id' => 3,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 19,
            'kpi_id' => 4,
            'effect' => 1.00,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 20,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 20,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 20,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 20,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 21,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 21,
            'kpi_id' => 2,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 21,
            'kpi_id' => 3,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 21,
            'kpi_id' => 4,
            'effect' => 0.96,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 22,
            'kpi_id' => 1,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 22,
            'kpi_id' => 2,
            'effect' => 0.92,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 22,
            'kpi_id' => 3,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 22,
            'kpi_id' => 4,
            'effect' => 0.96,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 23,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 23,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 23,
            'kpi_id' => 3,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 23,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 24,
            'kpi_id' => 1,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 24,
            'kpi_id' => 2,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 24,
            'kpi_id' => 3,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 24,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 25,
            'kpi_id' => 1,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 25,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 25,
            'kpi_id' => 3,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 25,
            'kpi_id' => 4,
            'effect' => 1.00,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 26,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 26,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 26,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 26,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 27,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 27,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 27,
            'kpi_id' => 3,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 27,
            'kpi_id' => 4,
            'effect' => 0.96,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 28,
            'kpi_id' => 1,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 28,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 28,
            'kpi_id' => 3,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 28,
            'kpi_id' => 4,
            'effect' => 1.00,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 29,
            'kpi_id' => 1,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 29,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 29,
            'kpi_id' => 3,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 29,
            'kpi_id' => 4,
            'effect' => 0.96,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 30,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 30,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 30,
            'kpi_id' => 3,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 30,
            'kpi_id' => 4,
            'effect' => 1.00,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 31,
            'kpi_id' => 1,
            'effect' => 0.96,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 31,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 31,
            'kpi_id' => 3,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 31,
            'kpi_id' => 4,
            'effect' => 1.00,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 32,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 32,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 32,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 32,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 33,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 33,
            'kpi_id' => 2,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 33,
            'kpi_id' => 3,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 33,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 34,
            'kpi_id' => 1,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 34,
            'kpi_id' => 2,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 34,
            'kpi_id' => 3,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 34,
            'kpi_id' => 4,
            'effect' => 0.96,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 35,
            'kpi_id' => 1,
            'effect' => 1.00,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 35,
            'kpi_id' => 2,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 35,
            'kpi_id' => 3,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 35,
            'kpi_id' => 4,
            'effect' => 1.08,
        ]);

        DB::table('answer_kpi')->insert([
            'answer_id' => 36,
            'kpi_id' => 1,
            'effect' => 1.04,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 36,
            'kpi_id' => 2,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 36,
            'kpi_id' => 3,
            'effect' => 1.08,
        ]);
        DB::table('answer_kpi')->insert([
            'answer_id' => 36,
            'kpi_id' => 4,
            'effect' => 1.04,
        ]);
    }
}
