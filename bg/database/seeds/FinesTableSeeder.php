<?php

use Illuminate\Database\Seeder;

class FinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fines')->insert([
            'question_id' => 3,
            'title' => "Banken krijgen boete voor overtreden privacywetgeving",
            'news' => "Banken hebben de regels van de Autoriteit Financiële Markten (AFM)
            overschreden door gevoelige klantinformatie door te verkopen aan derden
            zonder dat de klanten hiervan op de hoogte waren. Dit is volledig in strijd
            met de wet bescherming persoonsgegevens (Wbp).
            <br /><b>De banken die zich schuldig hebben gemaakt aan het verkopen van
            gevoelige informatie krijgen van de AFM een boete van €10 miljoen.</b>",
        ]);
        DB::table('fines')->insert([
            'question_id' => 5,
            'title' => "GMPK failliet verklaard",
            'news' => "Het betrouwbaar ogende adviesbureau GMPK blijkt al jarenlang zwaar
            verliesgevend te zijn. Dat blijkt uit recentelijk onderzoek van de AFM. Uit
            het onderzoek blijkt dat de cijfers van het voorheen gerenommeerde
            adviesbureau de afgelopen jaren gemanipuleerd werden door de interne
            accountantsdienst en de externe accountant onder één hoedje speelde
            met GMPK. Een nog onbekende klokkenluider heeft het hele verhaal uit de
            doeken gedaan. Na een analyse van de AFM is gebleken dat GMPK niet
            onder een faillissement uit gaat komen. Een select gedeelte van de
            aandeelhouders lijkt zijn geld terug te kunnen krijgen, maar de overige
            schuldeisers zien hun investering in rook opgaan.
            <br /><b>De banken die gebruik hebben gemaakt van de diensten van GMPK
            realiseren geen omzet / besparing en zullen door het faillissement de
            vooruitbetaalde kosten niet terug kunnen vorderen.</b>",
        ]);
        DB::table('fines')->insert([
            'question_id' => 8,
            'title' => "Nederlandse banken investeren in wapenbedrijven",
            'news' => "Zes Nederlandse banken investeren geld in bedrijven die controversiële
            wapens als clusterbommen maken en wapens leveren aan foute regimes.
            Dat stellen Oxfam Novib, Amnesty International, FNV en Milieudefensie op
            basis van onderzoek dat in opdracht van hen is gedaan.
            <br /><b>De banken die zich schuldig hebben gemaakt aan investeringen in
            wapenbedrijven krijgen van de DNB een boete van €8 miljoen
            opgelegd.</b>",
        ]);
        DB::table('fines')->insert([
            'question_id' => 10,
            'title' => "DNB legt een bestuurlijke boete op van EUR 15 miljoen",
            'news' => "De Nederlandsche Bank (DNB) heeft een bestuurlijke boete van EUR 15
            miljoen opgelegd aan een aantal Nederlandse banken. De banken dienen
            volgens toepasselijke regelgeving te allen tijde minimaal aan een totale
            kapitaalratio van 120% te voldoen. Door achtergestelde leningen van
            financiële instellingen niet, zoals de wet- en regelgeving voorschrijft, af te
            trekken van haar toetsingsvermogen hebben de banken de wettelijke
            minimale totale kapitaal c.q. solvabiliteitsratio van 120% doorbroken.
            <br /><b>De banken die niet voldoen aan de minimale kapitaalratio van 120%
            krijgen een boete van EUR 15 miljoen opgelegd.</b>",
        ]);
    }
}
