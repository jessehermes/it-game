<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            'question' => "De bank overweegt om de acceptgiro af te schaffen, aangezien dit niet meer van deze tijd is. Wat doen jullie?",
        ]);
        DB::table('questions')->insert([
            'question' => "Tegenwoordig gebeurt bankieren grotendeels digitaal. Een interne partij oppert het idee om gefaseerd alle lokale vestigingen van de bank te sluiten, zodat alleen het hoofdkantoor nog fysiek blijft bestaan. Wat doen jullie?",
        ]);
        DB::table('questions')->insert([
            'question' => "Social media speelt een steeds grotere rol in het verkrijgen van informatie over consumenten. De bank overweegt meer gebruik te maken van Social Media.",
        ]);
        DB::table('questions')->insert([
            'question' => "De bank overweegt om in zee te gaan met het FinTech bedrijf \"e-Lening\", dat zich bezig houdt met instant lending. Dat wil zeggen het verstrekken van leningen van maximaal 100.000 euro aan mkb-bedrijven. Hierbij gebruikt het de nieuwste technologie. Het bedrijf is zeer efficiënt, maar erg volatiel. Een interessante business partner voor de bank of niet?",
        ]);
        DB::table('questions')->insert([
            'question' => "Om een kostenbesparing te realiseren wil de bank medewerkers ontslaan. Het doel is verdergaande digitalisering van werkzaamheden.",
        ]);
        DB::table('questions')->insert([
            'question' => "Tijdens een vergadering wordt het idee geopperd om een crowdfunding platform / crowdfunding fonds op te richten. Op deze manier kunnen investeerders start-up bedrijven financieren en ontvangt de bank een fee. Het volledige risico is normaal gesproken voor de investeerder.",
        ]);
        DB::table('questions')->insert([
            'question' => "De bank overweegt de allernieuwste biometrische authenticatie methode te gaan implementeren. Wachtwoorden en pincodes worden vervangen door onder andere de vingerafdruk en stemherkenning. Veelbelovend, maar ook iets wat nog maar in de kinderschoenen staat. Wat doen jullie?",
        ]);
        DB::table('questions')->insert([
            'question' => "Maatschappelijk verantwoord bankieren staat steeds meer centraal. De bank heeft de keuze om te investeren in duurzame fondsen waarbij enkel in fondsen wordt geïnvesteerd die goed zijn voor het milieu.",
        ]);
        DB::table('questions')->insert([
            'question' => "De bank overweegt om Blockchain te gaan gebruiken. Wat beslissen jullie?",
        ]);
        DB::table('questions')->insert([
            'question' => "De bank overweegt om haar kapitaalbuffers te versterken. Om dit re realiseren worden enkele acties overwogen.",
        ]);
        DB::table('questions')->insert([
            'question' => "De bank overweegt om betalingsverkeer met Digital Currencies (zoals de Bitcoin) te gaan ondersteunen. De bank gaat dan dus digital currencies accepteren als betaalmiddel. Bespreek de voor- en nadelen en beslis wat jullie doen.",
        ]);
        DB::table('questions')->insert([
            'question' => "De bank heeft dit jaar veel winst gemaakt en overweegt hiermee een innovatieve investering te doen. Waarin  investeren jullie?",
        ]);
    }
}
