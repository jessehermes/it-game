<?php

use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answers')->insert([
            'question_id' => 1,
            'answer' => "De bank schaft de acceptgiro af.",
            'cost' => 5,
            'revenue' => 8,
        ]);
        DB::table('answers')->insert([
            'question_id' => 1,
            'answer' => "De bank schaft de acceptgiro niet af.",
            'cost' => 0,
            'revenue' => 0,
        ]);
        DB::table('answers')->insert([
            'question_id' => 1,
            'answer' => "De bank schaft de acceptgiro gedeeltelijk af, maar zal de acceptgiro voor bepaalde doelgroepen zoals ouderen wel blijven gebruiken.",
            'cost' => 15,
            'revenue' => 11,
        ]);
        
        DB::table('answers')->insert([
            'question_id' => 2,
            'answer' => "Geen sprake van. De kantoren blijven voorlopig geopend.",
            'cost' => 0,
            'revenue' => -6,
        ]);
        DB::table('answers')->insert([
            'question_id' => 2,
            'answer' => "Een aantal kantoren zullen gesloten worden, maar in de grote en belangrijke plaatsen zal de bank een lokale vestiging houden.",
            'cost' => 7,
            'revenue' => 8,
        ]);
        DB::table('answers')->insert([
            'question_id' => 2,
            'answer' => "Alle lokale vestigingen worden op korte termijn gesloten.",
            'cost' => 15,
            'revenue' => 20,
        ]);
        
        DB::table('answers')->insert([
            'question_id' => 3,
            'answer' => "De bank investeert in Social Media dat zal worden gebruikt voor informatieverstrekking aan de klant.",
            'cost' => 7,
            'revenue' => 5,
        ]);
        DB::table('answers')->insert([
            'question_id' => 3,
            'answer' => "De bank investeert in Social Media en verkoopt de verkregen data tegen hoge tarieven aan derden.",
            'cost' => 10,
            'revenue' => (12 - 10), //Fine of 10
        ]);
        DB::table('answers')->insert([
            'question_id' => 3,
            'answer' => "Social Media hoort niet thuis binnen de dienstverlening van de bank.",
            'cost' => 0,
            'revenue' => 0,
        ]);
        
        DB::table('answers')->insert([
            'question_id' => 4,
            'answer' => "De meeste FinTech bedrijven zijn, lettende op alle vereisten (ratio's) waar banken aan moeten voldoen, te volatiel om in te investeren. De bank besluit daarom geen gebruik te maken van de diensten van FinTech bedrijven.",
            'cost' => 0,
            'revenue' => 0,
        ]);
        DB::table('answers')->insert([
            'question_id' => 4,
            'answer' => "De bank gaat gebruik maken van de diensten van FinTech bedrijven.",
            'cost' => 5,
            'revenue' => 10,
        ]);
        DB::table('answers')->insert([
            'question_id' => 4,
            'answer' => "De bank besluit het bedrijf op te kopen en te integreren met de bank.",
            'cost' => 12,
            'revenue' => 20,
        ]);
        
        DB::table('answers')->insert([
            'question_id' => 5,
            'answer' => "De bank hanteert een sociaal beleid en ontslaat geen medewerkers.",
            'cost' => 0,
            'revenue' => -15,
        ]);
        DB::table('answers')->insert([
            'question_id' => 5,
            'answer' => "De bankt ontslaat 5% van de bestaande hoeveelheid medewerkers.",
            'cost' => 8,
            'revenue' => 16,
        ]);
        DB::table('answers')->insert([
            'question_id' => 5,
            'answer' => "Adviesbureau GMPK wordt ingehuurd om de impact van een eventuele ontslaggolf te analyseren. GMPK eist vooruitbetaling van de projectkosten.",
            'cost' => 10,
            'revenue' => 0,
        ]);
        
        DB::table('answers')->insert([
            'question_id' => 6,
            'answer' => "De bank ziet het nut van dit platform niet in en doet niks.",
            'cost' => 0,
            'revenue' => 0,
        ]);
        DB::table('answers')->insert([
            'question_id' => 6,
            'answer' => "De bank zet een platform op, maar besluit de investeerder tegemoet te komen en een gedeelte van het risico af te dekken.",
            'cost' => 7,
            'revenue' => 10,
        ]);
        DB::table('answers')->insert([
            'question_id' => 6,
            'answer' => "De bank zet een platform op en vangt zo alleen een fee voor de bemiddeling tussen de investeerder en de start-up bedrijven.",
            'cost' => 5,
            'revenue' => 12,
        ]);
        
        DB::table('answers')->insert([
            'question_id' => 7,
            'answer' => "Dit is erg riskant en breekt te veel in op de privacy van de klant. De bank doet niks.",
            'cost' => 0,
            'revenue' => 0,
        ]);
        DB::table('answers')->insert([
            'question_id' => 7,
            'answer' => "De bank gaat klanten de optie geven om voor deze biometrische authenticatie te kiezen, maar behoudt ook de ouderwetse wachtwoorden en pincodes.",
            'cost' => 5,
            'revenue' => 10,
        ]);
        DB::table('answers')->insert([
            'question_id' => 7,
            'answer' => "Pincodes en wachtwoorden zijn niet meer van deze tijd. De vingerafdruk en stemherkenning worden de nieuwe standaard.",
            'cost' => 8,
            'revenue' => 12,
        ]);
        
        DB::table('answers')->insert([
            'question_id' => 8,
            'answer' => "De bank kiest voor de huidige strategie waarbij wordt geïnvesteerd in de meest rendabele fondsen ongeacht de achtergrond.",
            'cost' => 0,
            'revenue' => (12 - 8), //Fine of 8
        ]);
        DB::table('answers')->insert([
            'question_id' => 8,
            'answer' => "De klanten kunnen kiezen of zij willen investeren in duurzame fondsen.",
            'cost' => 7,
            'revenue' => 0,
        ]);
        DB::table('answers')->insert([
            'question_id' => 8,
            'answer' => "De bank gaat volledig over op investeren in duurzame fondsen.",
            'cost' => 12,
            'revenue' => -5,
        ]);
        
        DB::table('answers')->insert([
            'question_id' => 9,
            'answer' => "De bank start een joint-venture met een blockchain partij om marktleider te worden van deze techniek.",
            'cost' => 15,
            'revenue' => 21,
        ]);
        DB::table('answers')->insert([
            'question_id' => 9,
            'answer' => "De bank gaat blockchain gebruiken.",
            'cost' => 6,
            'revenue' => 10,
        ]);
        DB::table('answers')->insert([
            'question_id' => 9,
            'answer' => "De bank besluit geen gebruik te willen maken van blockchain. Dit is te riskant.",
            'cost' => 0,
            'revenue' => -5,
        ]);
        
        DB::table('answers')->insert([
            'question_id' => 10,
            'answer' => "Verkopen van woningfinancieringen aan externe partijen.",
            'cost' => 10,
            'revenue' => 0,
        ]);
        DB::table('answers')->insert([
            'question_id' => 10,
            'answer' => "Verstrekken van kredieten enkel aan klanten binnen de focus van de bank.",
            'cost' => 6,
            'revenue' => 0,
        ]);
        DB::table('answers')->insert([
            'question_id' => 10,
            'answer' => "De kapitaalpositie van de bank is voldoende, er wordt geen actie ondernomen.",
            'cost' => 0,
            'revenue' => 5,
        ]);
        
        DB::table('answers')->insert([
            'question_id' => 11,
            'answer' => "Dit is te riskant, gezien de volatiliteit van digital currencies. De bank besluit af te zien van digital currencies.",
            'cost' => 0,
            'revenue' => 0,
        ]);
        DB::table('answers')->insert([
            'question_id' => 11,
            'answer' => "De bank gaat digital currencies accepteren als betaalmiddel.",
            'cost' => 7,
            'revenue' => 0,
        ]);
        DB::table('answers')->insert([
            'question_id' => 11,
            'answer' => "De bank gaat zelf een digitale munt invoeren met een zestal andere banken.",
            'cost' => 10,
            'revenue' => 0,
        ]);
        
        DB::table('answers')->insert([
            'question_id' => 12,
            'answer' => "De winst blijft binnen de bank waardoor het kapitaalratio en omzetpercentage stijgen.",
            'cost' => 0,
            'revenue' => 10,
        ]);
        DB::table('answers')->insert([
            'question_id' => 12,
            'answer' => "De bank gaat investeren in een compleet nieuw klantsysteem, waarbij de dienstverlening op maat wordt gemaakt voor de klant.",
            'cost' => 13,
            'revenue' => 13,
        ]);
        DB::table('answers')->insert([
            'question_id' => 12,
            'answer' => "De bank neemt een groot internationaal Fintech bedrijf over dat gespecialiseerd is in het verzorgen van betalingsverkeer.",
            'cost' => 25,
            'revenue' => 30,
        ]);
    }
}
