<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Glenn',
            'email' => 'glenn@haygarth.com',
            'password' => Hash::make('46378756722'),
        ]);
    }
}
