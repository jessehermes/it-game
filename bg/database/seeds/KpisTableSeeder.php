<?php

use Illuminate\Database\Seeder;

class kpisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kpis')->insert([
            'name' => "Kapitaal Ratio",
        ]);
        DB::table('kpis')->insert([
            'name' => "Reputatie",
        ]);
        DB::table('kpis')->insert([
            'name' => "Innovatie",
        ]);
        DB::table('kpis')->insert([
            'name' => "Klanttevredenheid",
        ]);
    }
}
