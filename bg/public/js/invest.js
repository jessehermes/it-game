/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 228);
/******/ })
/************************************************************************/
/******/ ({

/***/ 228:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(229);


/***/ }),

/***/ 229:
/***/ (function(module, exports) {

$(document).ready(function () {

    var x = document.getElementById("investCard");
    x.style.display = "none";
    document.getElementById("placeholder").innerHTML = "Wacht tot de game start.";

    var investHeader = document.getElementById("investHeader");
    investHeader.innerHTML = "Invest in kpi's - Total budget: " + invest_budget;

    navbarTitle = document.getElementById("appName");
    navbarTitle.innerHTML = game_session.name;

    createSliders();
    updateTotal();

    var investForm = document.getElementById("investForm");
    investForm.onsubmit = function validateInvestment() {

        if (sliderTotal() != invest_budget) {
            window.alert("Totale investering is niet " + invest_budget);
            return false;
        }

        return true;
    };

    getCurrentStage(game_session.id);

    setInterval(function () {
        getCurrentStage(game_session.id);
    }, 3000);
});

function getCurrentStage(gameid) {
    $.get("info/current-stage/" + gameid).done(function (data) {
        if (data > 0) {
            var x = document.getElementById("investCard");
            x.style.display = "block";
            document.getElementById("placeholder").innerHTML = "";
        }
    });
}

function updateTotal() {
    var totalInvestment = document.getElementById("totalInvestment");
    while (totalInvestment.firstChild) {
        totalInvestment.removeChild(totalInvestment.firstChild);
    }
    totalInvestment.appendChild(document.createTextNode("Total investment is: " + sliderTotal()));
}

function sliderTotal() {
    var sliderValues = document.getElementsByName("slider[]");
    var total = 0;

    for (i = 0; i < sliderValues.length; i++) {
        total += parseInt(sliderValues[i].value);
    }

    return total;
}

function createSliders() {
    var sliderContainer = document.getElementById("slidercontainer");
    for (var i = 0; i < kpis.length; i++) {

        var sliderDiv = document.createElement("div");

        var text = document.createElement("p");
        text.appendChild(document.createTextNode(kpis[i].name));

        var slider = document.createElement("input");
        slider.type = "range";
        slider.id = "slider" + i;
        slider.name = "slider[]";
        slider.min = 0;
        slider.max = 50;
        slider.value = 0;
        slider.style = "width: 90%";

        var span = document.createElement("span");
        span.id = "sliderSpan" + i;
        span.appendChild(document.createTextNode(slider.value));

        slider.oninput = function () {
            idnumber = this.id.match(/\d+/)[0];
            var span2 = document.getElementById("sliderSpan" + idnumber);

            span2.innerHTML = this.value;

            updateTotal();
        };

        sliderDiv.appendChild(text);
        sliderDiv.appendChild(slider);
        sliderDiv.appendChild(span);
        sliderContainer.appendChild(sliderDiv);
    }
}

// function testGraph()
// {
//     var ctx = document.getElementById("myChart").getContext('2d');
//     var myChart = new Chart(ctx, {
//         type: 'bar',
//         data: {
//             labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
//             datasets: [{
//                 label: '# of Votes',
//                 data: [12, 19, 3, 5, 2, 3],
//                 backgroundColor: [
//                     'rgba(255, 99, 132, 0.2)',
//                     'rgba(54, 162, 235, 0.2)',
//                     'rgba(255, 206, 86, 0.2)',
//                     'rgba(75, 192, 192, 0.2)',
//                     'rgba(153, 102, 255, 0.2)',
//                     'rgba(255, 159, 64, 0.2)'
//                 ],
//                 borderColor: [
//                     'rgba(255, 99, 132, 1)',
//                     'rgba(54, 162, 235, 1)',
//                     'rgba(255, 206, 86, 1)',
//                     'rgba(75, 192, 192, 1)',
//                     'rgba(153, 102, 255, 1)',
//                     'rgba(255, 159, 64, 1)'
//                 ],
//                 borderWidth: 1
//             }]
//         },
//         options: {
//             scales: {
//                 yAxes: [{
//                     ticks: {
//                         beginAtZero:true
//                     }
//                 }]
//             }
//         }
//     });
// }

// function testGraph2()
// {
//     Chart.defaults.line.spanGaps = true;

//     var ctx = document.getElementById("myChart").getContext('2d');
//     var myChart = new Chart(ctx, {
//         type: 'line',
//         data: {
//           labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
//           datasets: [{
//             label: 'apples',
//             data: [12, 19, 3, 17, 6, 3, 7],
//             backgroundColor: "rgba(153, 255, 51, 0.4)"
//           }, {
//             label: 'oranges',
//             data: [2, 29, 5, 5, 2, 3, 10],
//             backgroundColor: "rgba(255, 153, 0, 0.4)"
//           }]
//         }
//       });
// }

/***/ })

/******/ });