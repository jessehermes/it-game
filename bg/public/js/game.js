/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 226);
/******/ })
/************************************************************************/
/******/ ({

/***/ 226:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(227);


/***/ }),

/***/ 227:
/***/ (function(module, exports) {

var currentQuestion = -1;
var answerCosts = [];
var budget;
var questionShown = false;
var fineUpdated = false;

$(document).ready(function () {

    navbarTitle = document.getElementById("appName");
    navbarTitle.innerHTML = game_session.name;

    hideQuestion();
    updateKpi(team_id);

    var answerForm = document.getElementById("answerForm");
    answerForm.onsubmit = function checkBudget() {

        for (var i = 0; i < 20; i++) // TODO: 20 should be amount of answers 
        {
            var answerId = "answer" + i;
            if (document.getElementById(answerId).checked) {
                if (answerCosts[i] <= budget) {
                    return true;
                } else {
                    window.alert("Niet genoeg budget!");
                    return false;
                }
            }
        }

        return false;
    };

    getCurrentQuestionNumber(game_session.id);

    setInterval(function () {
        getCurrentQuestionNumber(game_session.id);
    }, 5000);
});

function hideQuestion() {
    var x = document.getElementById("question-container");
    x.style.display = "none";
    questionShown = false;
}

function showQuestion() {
    var x = document.getElementById("question-container");
    x.style.display = "block";
    questionShown = true;
}

function updateNews(gameid) {

    $.get("info/news-update/" + gameid).done(function (data) {

        document.getElementById("overlay-text").innerHTML = data.news;

        if (data.update && questionShown) {
            newsShow();
        }
    });
}

function updateFine(gameid) {

    $.get("info/fine-update/" + gameid).done(function (data) {

        if (data.hasFine) {
            document.getElementById("fine-overlay-text").innerHTML = data.fine;
            fineShow();
        }
    });
}

function getCurrentQuestionNumber(gameid) {
    $.get("info/current-question-number/" + gameid).done(function (data) {

        if (data.questionStage == 5) {
            // final question is finished watch host
            return;
        }

        // Check if there is a new question & if the player can view it yet
        if (currentQuestion != data.currentQuestion && data.questionStage != 0) {
            fineUpdated = false;
            updateQuestion(gameid);
            updateKpi(team_id);
            showQuestion();
            updateNews(gameid);

            currentQuestion = data.currentQuestion;
        }

        // Feedback before game starts
        if (data.currentQuestion == -1) {
            document.getElementById("temp").innerHTML = "Wacht tot de game start.";
        } else {
            var elem = document.getElementById('temp');
            if (elem != null) {
                elem.parentNode.removeChild(elem);
            }
        }

        // Hide question if it has been answered
        if (question_answered == currentQuestion) {
            hideQuestion();
            if (!fineUpdated) {
                fineUpdated = true;
                updateFine(gameid);
            }
        }
    });
}

function updateQuestion(gameid) {
    $.get("info/current-question/" + gameid).done(function (data) {

        //Check if current question exists
        if (!data) {
            //TODO: give the player feedback
            return;
        }

        document.getElementById("question").innerHTML = data.question;
        document.getElementById("questionid").value = data.id;

        var form = createAnswerForm(data.answers);
    });
}

function createAnswerForm(array) {

    var answerContainer = document.getElementById("answer-form");
    while (answerContainer.firstChild) {
        answerContainer.removeChild(answerContainer.firstChild);
    }

    alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q"];

    for (var i = 0; i < array.length; i++) {

        var el = document.createElement("input");
        el.id = "answer" + i;
        el.type = "radio";
        el.name = "answer";
        el.value = array[i].id;
        answerCosts[i] = array[i].cost;

        var label = document.createElement("Label");

        label.htmlFor = i;
        label.innerHTML = alphabet[i] + ": Kost " + array[i].cost + ": " + array[i].answer;

        answerContainer.appendChild(el);
        answerContainer.appendChild(label);
        answerContainer.appendChild(document.createElement("BR"));
    }
}

function updateKpi(teamid) {

    var ctx = document.getElementById("myChart").getContext('2d');
    var datasets = [];
    var colors = ["rgba(250, 30, 30, 0.8)", "rgba(30, 250, 30, 0.8)", "rgba(30, 30, 250, 0.8)", "rgba(200, 30, 200, 0.8)", "rgba(200, 200, 30, 0.8)", "rgba(30, 200, 200, 0.8)"];

    var ctx2 = document.getElementById("budgetChart").getContext('2d');

    $.get("info/financialkpi/" + teamid).done(function (data) {
        var financialData = data;
        budget = data[data.length - 1].budget;

        var myChart2 = new Chart(ctx2, {
            type: 'horizontalBar',
            data: {
                datasets: [{
                    label: 'Budget',
                    backgroundColor: 'rgba(250, 30, 30, 0.8)',
                    data: [budget]
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            max: 100
                        }
                    }]
                }
            }
        });

        $.get("info/kpi/" + teamid).done(function (data) {

            for (var i = 0; i < data.length; i++) {
                // loop through kpis
                var values = [];
                var labels = [];
                for (var j = 0; j < data[i].values.length; j++) {
                    // loop through rounds
                    values[j] = data[i].values[j].value;
                    labels[j] = data[i].values[j].round;
                }
                datasets[i] = { "label": data[i].name, "data": values, "backgroundColor": colors[i], "borderColor": colors[i], "fill": false };
            }

            var values = [];
            var labels = [];
            for (var i = 0; i < financialData.length; i++) {
                // loop through rounds
                values[i] = financialData[i].ratio * 100;
                labels[i] = i;
            }

            datasets[data.length] = { "label": "Omzet/Kosten", "data": values, "backgroundColor": colors[colors.length - 1], "borderColor": colors[colors.length - 1], "fill": false };

            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: datasets
                },
                options: {
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'score'
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'ronde'
                            }
                        }]
                    }
                }
            });
        });
    });
}

/***/ })

/******/ });