<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game_session extends Model
{
    protected $fillable = [
        'current_stage', 'current_question', 'question_stage', 'name'
    ];

    public function questions()
    {
        return $this->belongsToMany('App\Question');
    }

    public function teams()
    {
        return $this->hasMany('App\Team');
    }
}
