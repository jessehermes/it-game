<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'question_id', 'answer', 'cost', 'revenue', 'explanation',
    ];

    public function kpis()
    {
        return $this->belongsToMany('App\Kpi')->withPivot('effect', 'explanation');
    }
}
