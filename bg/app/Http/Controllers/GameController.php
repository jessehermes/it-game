<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Game_session;
use App\Team;
use App\Answer;
use App\Kpi;

class GameController extends Controller
{
    public function index($error = null)
    {
        return view('join', [
            'error' => $error
            ]
        );
    }

    public function investGame()
    {
        $game_id = $_POST["gameid"];
        $team_id = $_POST["teamid"];

        $game_session = Game_session::find($game_id);

        $range_slider_value = $_POST["slider"];

        $kpis = Kpi::all();

        $totalInvestment = 0;
        $maxInvestment = 85; //TODO: Base this of something/fetch this from the database~~~

        for($i = 0; $i < sizeof($range_slider_value); $i++)
        {
            $totalInvestment += (int)$range_slider_value[$i];
        }

        // If investment budget is too high return the invest screen
        if ($totalInvestment > $maxInvestment)
        {
            return view('invest', compact('game_session', 'kpis'), ['teamid' => $team_id, 'investbudget' => $maxInvestment]);
        }

        for ($i = 0; $i < sizeof($kpis); $i++) 
        {
            if (DB::table('kpi_team')->where('team_id', $team_id)->where('kpi_id', $kpis[$i]->id)->exists())
            {
                //this investment already exists
            }
            else
            {
                DB::table('kpi_team')->insert([
                    'team_id' => $team_id,
                    'kpi_id' => $kpis[$i]->id,
                    'investment' => (int)$range_slider_value[$i]
                ]);
            }
        }

        return view('game', compact('game_session'), ['teamid' => $team_id, 'questionanswered' => -1]);
    }

    public function getCurrentStage($game_id)
    {
        $game_session = Game_session::find($game_id);

        $current_stage = $game_session->current_stage;

        return $current_stage;
    }

    public function joinGame()
    {
        $game_id = $_POST["game_id"];
        $team_name = $_POST["team_name"];

        if (!$game_id)
        {
            return $this->index();
        }

        if (DB::table('game_sessions')->where('id', $game_id)->exists())
        {
            $game_session = Game_session::find($game_id);

            $current_stage = $game_session->current_stage;

            if ($current_stage != 0)
            {
                return $this->index("Game is in progress or finished");
            }

            $teams = Game_session::find($game_id)->teams;

            foreach ($teams as $team)
            {
                if ($team->team_name == $team_name)
                {
                    return $this->index("Team name is taken");
                }
            }

            $team = new Team;
            $team->game_session_id = $game_id;
            $team->team_name = $team_name;
            $team->save();

            $kpis = Kpi::all();
            $invest_budget = 85; //TODO: Base this of something/fetch this from the database~~~

            return view('invest', compact('game_session', 'kpis'), ['teamid' => $team->id, 'investbudget' => $invest_budget]);
        }

        return $this->index("Game id doesn't exist");
    }

    public function getFine($game_id)
    {
        $session = Game_session::find($game_id);
        $questions = $session->questions;
        $currentQuestion = $session->current_question;
        $fine = "";
        $i = 0;

        // $question = $questions->slice($currentQuestion, 1);
        foreach($questions as $question)
        {
            if($i == $currentQuestion && $question->fines != null)
            {
                $fine = "<h4>Boetebericht:</h4><h2>" . $question->fines['title'] . "</h2>" . $question->fines['news'];
                
                return ["fine" => $fine, "hasFine" => true];
            }

            $i++;
        }

        return ["fine" => $fine, "hasFine" => false];
    }

    public function getNews($game_id)
    {
        $session = Game_session::find($game_id);
        $questions = $session->questions;
        $questionCount = $session->questions->Count();
        $currentQuestion = $session->current_question;
        $news = "";
        $i = 0;
        $lookback = 1; // if this number is 3, it means at question 5 the news article from question 2 dissapears
        $lookahead = 2; // if this number is 1, it means at question 1 the news article from question 2 is visible
        $newNews = false;

        foreach($questions as $question)
        {
            if ($question->news != NULL && $i > $currentQuestion - $lookback)
            {
                $news = "<h2>" . $question->news['title'] . "</h2>" . $question->news['news'] . "\n\n" . $news;
                $newNews = true;
            }

            $i++;
            if ($i > $currentQuestion + $lookahead)
            {
                $news = "<h4>Recente Nieuwsberichten:</h4>" . $news;
                return ["news" => $news, "update" => $newNews];
            }
            $newNews = false;
        }

        $news = "<h4>Recente Nieuwsberichten:</h4>" . $news;
        return ["news" => $news, "update" => $newNews];
    }

    public function getCurrentQuestionNumber($game_id)
    {
        $currentQuestion = Game_session::find($game_id)->current_question;
        $questionStage = Game_session::find($game_id)->question_stage;

        $jsonQuestion = [
            'currentQuestion' => $currentQuestion,
            'questionStage' => $questionStage
        ];

        return $jsonQuestion;
    }

    public function getQuestion($game_id)
    {
        $session = Game_session::find($game_id);
        $currentQuestion = $session->current_question;

        $questions = $session->questions()->get();

        // Check if the current question exists
        if ($questions->count() <= $currentQuestion || $currentQuestion == -1)
        {
            $session->current_stage = 5;
            $session->question_stage = 5;
            $session->save();
            return ["game_finished" => true];
        }

        $question = $questions[$currentQuestion];

        $answers = $question->answers;

        $jsonAnswers = [];
        foreach ($answers as $answer)
        {
            $jsonAnswers[] = [
                'id' => $answer->id,
                'answer' => $answer->answer,
                'cost' => $answer->cost
            ];
        }

        $jsonQuestion = [
            'id' => $question->id,
            'question' => $question->question,
            'answers' => $jsonAnswers
        ];

        return $jsonQuestion;
    }

    public function answer()
    {
        $answer_id = $_POST["answer"];
        $question_id = $_POST["question"];
        $game_id = $_POST["gameid"];
        $team_id = $_POST["teamid"];

        //check if the question has already been answered by the team, then update or insert accordingly
        if (DB::table('answer_team')->where('game_session_id', $game_id)->where('question_id', $question_id)->where('team_id', $team_id)->exists())
        {
            DB::table('answer_team')->where('game_session_id', $game_id)->where('question_id', $question_id)->where('team_id', $team_id)->update(['answer_id' => $answer_id]);
        }
        else
        {
            DB::table('answer_team')->insert([
                'game_session_id' => $game_id,
                'question_id' => $question_id,
                'team_id' => $team_id,
                'answer_id' => $answer_id
            ]);
        }

        $game_session = Game_session::find($game_id);
        return view('game', compact('game_session'), ['teamid' => $team_id, 'questionanswered' => $game_session->current_question]);
    }

    public function rejoin($team_id)
    {
        $team = Team::find($team_id);
        
        $game_session = Game_session::find($team->game_session_id);

        $answers = $team->answers()->count();

        return view('game', compact('game_session'), ['teamid' => $team_id, 'questionanswered' => $answers  - 1]);
    }

    public function kpi($team_id)
    {
        $base = 100;
        
        $kpis = Kpi::all();

        $kpiAllValues = [];
        $answers = Team::find($team_id)->answers()->get();

        // loop thorugh all kpis
        foreach($kpis as $kpi) {
            $kpiValues = [];
            $i = 0;
            $kpiValues[] = [
                "round" => $i,
                "value" => $base,
            ];
            // loop through all answers
            foreach($answers as $answer) {
                // loop through the kpis that belong to an answer
                foreach ($answer->kpis as $kpi2) {
                    // check if the kpi that belongs to the answer matches the kpi currently being calculated
                    if ($kpi2->pivot->kpi_id == $kpi->id) {
                        $bonus = 1;
                        if (DB::table('kpi_team')->where('team_id', $team_id)->where('kpi_id', $kpi->id)->exists()) {
                            $bonus = DB::table('kpi_team')->where('team_id', $team_id)->where('kpi_id', $kpi->id)->value('investment');
                            $bonus = ($bonus / 1500) + 1;
                        }
                        $newValue;

                        // check if the bonus has to be applied
                        if ($kpi2->pivot->effect > 1) {
                            $newValue = $kpiValues[$i]["value"] * $kpi2->pivot->effect * $bonus;
                            $newValue = round($newValue, 2);
                        }
                        else {
                            $newValue = $kpiValues[$i]["value"] * $kpi2->pivot->effect;
                            $newValue = round($newValue, 2);
                        }
                        $i++;
                        // store the kpi value of this round
                        $kpiValues[] = [
                            "round" => $i,
                            "value" => $newValue,
                        ];
                    }
                }
            }

            $kpiAllValues[] = [
                'name' => $kpi->name,
                'values' => $kpiValues,
            ];
        }

        return $kpiAllValues;
    }

    public function financialKpi($team_id)
    {
        $game_id = Team::find($team_id)->game_session_id;
        $questions = Game_session::find($game_id)->questions()->get();
        $numberOfQuestions = sizeof($questions);

        $budget = 4 + ($numberOfQuestions * 8);
        $totalcost = 100;
        $totalrevenue = 100;

        $jsonObject = [];
        $jsonObject[] = [
            'budget' => $budget,
            'cost' => $totalcost,
            'revenue' => $totalrevenue,
            'ratio' => round($totalrevenue / $totalcost, 2),
        ];
        
        $answers = Team::find($team_id)->answers()->get();

        foreach($answers as $answer)
        {
            $budget -= $answer->cost;
            $totalcost += $answer->cost;
            $totalrevenue += $answer->revenue;

            $jsonObject[] = [
                'budget' => $budget,
                'cost' => $totalcost,
                'revenue' => $totalrevenue,
                'ratio' => round($totalrevenue / $totalcost, 2),
            ];
        }

        return $jsonObject;
    }

    public function totalKpiScore($team_id)
    {
        $kpis = $this->kpi($team_id);
        $financialkpis = $this->financialKpi($team_id);
        $jsonTotal = [];

        for ($i=0; $i < sizeof($kpis[0]["values"]); $i++) {
            $roundtotal = 0;
            foreach($kpis as $kpi)
            {
                $roundtotal += $kpi["values"][$i]["value"];
            }

            $roundtotal += $financialkpis[$i]["ratio"] * 100;

            $jsonTotal[] = [
                "round" => $i,
                "value" => round($roundtotal / (Kpi::count() + 1), 2),
            ];
        }

        return $jsonTotal;
    }

    public function allTeamsKpi($game_id)
    {
        $teams = Game_session::find($game_id)->teams()->get();

        $jsonObject = [];

        foreach($teams as $team)
        {
            $jsonObject[] = [
                "team_name" => $team->team_name,
                "team_id" => $team->id,
                "kpis" => $this->kpi($team->id),
                "financialkpis" => $this->financialkpi($team->id),
                "totalkpis" => $this->totalKpiScore($team->id),
            ];
        }

        return $jsonObject;
    }
}
