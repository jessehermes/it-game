<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Question;
use App\Game_session;
use App\Kpi;

class HostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::All();
        return view('host', compact('questions'));
    }

    public function totalAnswers($gameid)
    {
        $game_session = Game_session::find($gameid);
        $current_question = $game_session->current_question;
        $teams = $game_session->teams;
        $questions = $game_session->questions()->get();

        // Check if the current question exists
        if ($questions->count() <= $current_question || $current_question < 0)
        {
            return ["game_finished" => true];
        }

        $question_id = $questions[$current_question]->id;

        $number_of_answers = 0;

        foreach($teams as $team)
        {
            if (DB::table('answer_team')->where('question_id', $question_id)->where('team_id', $team->id)->exists())
            {
                $number_of_answers++;
            }
        }

        $number_of_teams = $teams->count();

        $json_team_answers[] = [
            'number_of_teams' => $number_of_teams,
            'number_of_answers' => $number_of_answers,
        ];

        return $json_team_answers;
    }

    public function newSession()
    {
        $name = $_POST["name"];
        $value = $_POST["value"];

        $game_session = new Game_session;
        $game_session->current_stage = 0;
        $game_session->current_question = -1;
        $game_session->question_stage = 0;
        $game_session->name = $name;
        $game_session->save();

        if(!empty($_POST['questions'])) {
            foreach($_POST['questions'] as $check) {
                DB::table('game_session_question')->insert([
                    'game_session_id' => $game_session->id,
                    'question_id' => $check
                ]);
            }
        }

        return view('hosting', ['gameid' => $game_session->id]);
    }

    public function rejoinSession($gameid)
    {
        $game_session = Game_session::find($gameid);
        //TODO: check if the session exists

        if ($game_session->current_stage == 0)
        {
            return view('hosting', ['gameid' => $gameid]);
        }
        else if ($game_session->current_stage == 1)
        {
            return view('hostgame', compact('game_session'));
        }
        else if ($game_session->current_stage == 5)
        {
            return view('hostgame', compact('game_session'));
        }

        //TODO: if it gets here return an error
        return "error, stage number unknown";
    }

    public function showRapport($gameid)
    {
        $game_session = Game_session::find($gameid);

        $questions = $game_session->questions()->get();

        $rapport = "<h3>" . $game_session->name . " - id: " . $game_session->id . "</h3>";

        $i = 0;
        foreach($questions as $question)
        {
            $answers = $question->answers;

            $rapport = $rapport . "Ronde " . ++$i . ": " . $question->question . "<br /><ul>";

            foreach($answers as $answer)
            {
                $rapport = $rapport . "<li>" . $answer->answer . " - <b>" . $answer->explanation . "</b>";
                foreach($answer->kpis as $kpi)
                {
                    $rapport = $rapport . " - " . Kpi::find($kpi->pivot->kpi_id)->name . ": " . $kpi->pivot->effect;
                }
                $rapport = $rapport . "</li>";
            }
            $rapport = $rapport . "</ul>";
        }

        return view("rapport", ["rapport" => $rapport]);
    }

    public function showRules()
    {
        $gameid = $_POST["gameid"];

        $game_session = Game_session::find($gameid);

        $kpis = Kpi::all();
        $kpistring = "";
        $kpilist = "";
        $kpicount = $kpis->count();
        $i = 0;
        foreach($kpis as $kpi)
        {
            $i++;
            if ($i == $kpicount)
            {
                $kpistring = $kpistring . $kpi->name;
            }
            else
            {
                $kpistring = $kpistring . $kpi->name . ", ";
            }
            $kpilist = $kpilist . "<li>" . $kpi->name . "</li>";
        }
        $rounds = $game_session->questions->Count();
        $budget = 4 + ($rounds * 8);

        $rulesText = '<h1>' . $game_session->name . '</h1>
        Digitalisering, maatschappelijk verantwoord ondernemen, crowdfunding en social media. Ondernemen verandert! Van de nieuwste technologieën tot strengere wet- en regelgeving. Maar hoe ga je hier als onderneming op inspelen?  
        <br /><br /><b>De keuze is aan jou tijdens de ' . $game_session->name . '</b>
        <br /><br />Ervaar hoe het is om onderdeel te zijn van een management team van een grote onderneming. Samen met je collega’s kom je uitdagende scenario’s tegen waarin jullie als team strategische keuzes moeten maken. Deze keuzes hebben invloed op de Key Performance Indicators (KPI’s) van de onderneming, zoals ' . $kpistring . '. Lukt het jullie om innovatief en financieel gezond te zijn, terwijl je alle stakeholders tevreden houdt? 
        <br /><br /><h2>Speluitleg</h2> 
        <br /><b>Doelstelling</b> 
        <ul>
        <li>Ben jij de onderneming van de toekomst? In ' . $rounds . ' ronden dagen we je uit om de juiste beslissingen te nemen met je management team</li> 
        <li>Teamprestatie: met je team de beste gemiddelde score op alle KPI’s (gewogen gemiddelde)</li>
        <li>Persoonlijk: de hoogste score op je persoonlijke KPI wint als individu </li>
        </ul>
        <b>Aantal ronden</b> 
        <ul>
        <li>' . $rounds . ' ronden</li> 
        <li>Per ronde meerdere keuzemogelijkheden </li>
        <li>Nadenken over de gevolgen voor je KPI’s </li>
        <li>De spelleider bepaalt de tijd per ronde om de vraag te beantwoorden </li>
        </ul>
        <b>Key Performance Indicators (KPI’s) </b>
        <ul>
        <li>De KPI’s bepalen de score van je onderneming </li>
        <li>De volgende KPI’s doen mee met het spel: </li>
        <ul>
        ' . $kpilist . '
        </ul> 
        <li>Per KPI is er één teamlid verantwoordelijk</li> 
        </ul>
        <b>Budget</b> 
        <ul>
        <li>Tijdens het spel: Budget voor de ' . $rounds . ' ronden is EUR ' . $budget . ' miljoen</li>
        <li>Voorafgaand aan de ronden: Startbudget voor de beginsituatie is EUR 85 miljoen. Het startbudget moet volledig geïnvesteerd worden </li>
        <li>Resterend budget aan het einde van het spel levert niks op </li>
        </ul>
        <b>Nieuwsberichten</b> 
        <ul>
        <li>Nieuwsberichten beïnvloeden de keuze van de teams</li> 
        <li>De nieuwsberichten krijgen jullie binnen via de knop “nieuwsberichten” boven in je scherm </li>
        <li>Met het team overleggen jullie of deze informatie waardevol is voor de te nemen beslissingen </li>
        </ul>
        <b>Boetes</b> 
        <ul>
        <li>Bij verkeerde beslissingen kunnen boetes worden uitgedeeld </li>
        <li>Boetes hebben direct invloed op je omzet / kosten ratio, maar hebben geen invloed op het teambudget </li>
        </ul>
        <b>Startsituatie investeren</b> 
        <ul>
        <li>Keuze maken in lange termijn strategie van de onderneming </li>
        <li>Investeren in KPI’s gedurende het hele spel resulteert in een multiplier voor de KPI </li>
        <li>Multiplier is alleen van toepassing bij een goede keuze </li>
        </ul>
        <h2>Hermes Partners</h2>
        <b>Einde spel</b> 
        <ul>
        <li>Na ' . $rounds . ' ronden wordt er gekeken welk team er de beste score heeft en welk individu per team het beste heeft gescoord op zijn/haar persoonlijke KPI</li>
        </ul>';

        return view('rules', ['gameid' => $game_session->id, 'rulesText' => $rulesText]);
    }

    public function startInvest()
    {
        $gameid = $_POST["gameid"];

        $game_session = Game_session::find($gameid);

        if ($game_session->current_stage == 0)
        {
            $game_session->current_stage = 1;
            // $game_session->current_question = 0;
    
            $game_session->save();
        }

        $investText = "<h1>Startsituatie</h1> 
        <b>Investeren</b> 
        <ul>
        <li>Keuze maken in lange termijn strategie van de onderneming </li>
        <li>Investeren in KPI’s gedurende het hele spel resulteert in een multiplier voor de KPI </li>
        <li>Multiplier is alleen van toepassing bij een goede keuze </li>
        <li>Voorafgaand aan de ronden: Startbudget voor de beginsituatie is EUR 85 miljoen. Het startbudget moet volledig geïnvesteerd worden</li>
        </ul>
        <b>Voorbeeld</b> 
        <ul>
        <li>In het onderstaande voorbeeld hebben alle vier de teams dezelfde keuze gemaakt, die een positief effect had op de reputatie van de bank </li>
        <li>Het multiplier effect is te zien in het verschil in toename van de KPI reputatie </li>
        </ul>";

        return view('hostinvest', ['gameid' => $game_session->id, 'investText' => $investText]);
    }

    public function startSession()
    {
        $gameid = $_POST["gameid"];

        $game_session = Game_session::find($gameid);

        if ($game_session->current_question < 0)
        {
            // $game_session->current_stage = 1;
            $game_session->current_question = 0;
    
            $game_session->save();
        }

        return view('hostgame', compact('game_session'));
    }

    public function nextQuestion($gameid)
    {
        $game_session = Game_session::find($gameid);

        // TODO: Add a check to see if there is another question
        $game_session->current_question += 1;
        $game_session->question_stage = 0;

        $game_session->save();

        return compact('game_session');
    }

    public function showQuestion($gameid)
    {
        $game_session = Game_session::find($gameid);

        $game_session->question_stage = 1;
        $game_session->save();
        
        return compact('game_session');
    }

    public function getTeams($gameid)
    {
        $teams = Game_session::find($gameid)->teams;

        $jsonTeams = [];
        foreach ($teams as $team)
        {
            $jsonTeams[] = [
                'name' => $team->team_name,
            ];
        }

        return $jsonTeams;
    }
}
