<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Question;
use App\Answer;
use App\Kpi;
use App\News;
use App\Fine;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();
        $kpis = Kpi::all();
        $news = News::all();
        $fines = Fine::all();

        return view('admin/index', compact('questions', 'kpis', 'news', 'fines'));
    }

    public function addQuestion($numberofanswers)
    {
        $kpis = Kpi::all();
        return view('admin/newquestion', compact('kpis'), ['numberofanswers' => $numberofanswers]);
    }

    public function showQuestion($question_id)
    {
        $kpis = Kpi::all();

        $question = Question::find($question_id);

        $answers = $question->answers;

        return view('admin/updatequestion', compact('question', 'answers', 'kpis'));
    }

    public function updateQuestion()
    {
        if (empty($_POST["question"]) || empty($_POST["answers"]) || empty($_POST["costs"]) || empty($_POST["revenues"]) || empty($_POST["kpivalue"]) || empty($_POST["questionid"]) || empty($_POST["topic"]))
        {
            return "error, missing question info";
        }

        $question_id = $_POST["questionid"];
        $questionText = $_POST["question"];
        $questionTopic = $_POST["topic"];
        $answers = $_POST["answers"];
        $costs = $_POST["costs"];
        $revenues = $_POST["revenues"];
        $explanations = $_POST["explanations"];
        $kpivalues = $_POST["kpivalue"];

        for($i = 0; $i < count($kpivalues); $i++)
        {
            $kpivalues[$i] = (double)$kpivalues[$i];
        }

        // Find the question
        $question = Question::find($question_id);
        $question->question = $questionText; //user input
        $question->topic = $questionTopic; //user input
        $question->save();

        $existingAnswers = $question->answers;
        $kpis = Kpi::all();

        $i = 0;
        foreach($existingAnswers as $answer)
        {
            $answer->answer = $answers[$i];  //user input
            $answer->cost = $costs[$i];  //user input
            $answer->revenue = $revenues[$i];  //user input
            $answer->explanation = $explanations[$i];  //user input
            $answer->save();

            $j = 0;
            foreach($kpis as $kpi)
            {
                DB::table('answer_kpi')->where('answer_id', $answer->id)->where('kpi_id', $kpi->id)->update([
                    'effect' => $kpivalues[($j + $i * Kpi::count())],  //user input
                ]);
                $j++;
            }
            $i++;
        }

        return $this->index();
    }

    public function newQuestion()
    {
        if (empty($_POST["question"]) || empty($_POST["answers"]) || empty($_POST["costs"]) || empty($_POST["revenues"]) || empty($_POST["kpivalue"]) || empty($_POST["topic"]))
        {
            return "error, missing question info";
        }

        $questionText = $_POST["question"];
        $questionTopic = $_POST["topic"];
        $answers = $_POST["answers"];
        $costs = $_POST["costs"];
        $revenues = $_POST["revenues"];
        $explanations = $_POST["explanations"];
        $kpivalues = $_POST["kpivalue"];
        for($i = 0; $i < count($kpivalues); $i++)
        {
            $kpivalues[$i] = (double)$kpivalues[$i];
        }

        $kpis = Kpi::all();

        // Add the question
        $question = new Question;
        $question->question = $questionText; //user input
        $question->topic = $questionTopic; //user input
        $question->save();

        // Add the answers and the corresponding effects
        for($i = 0; $i < sizeof($answers); $i++)
        {
            $answer = new Answer;
            $answer->question_id = $question->id;
            $answer->answer = $answers[$i];  //user input
            $answer->cost = $costs[$i];  //user input
            $answer->revenue = $revenues[$i];  //user input
            $answer->explanation = $explanations[$i];  //user input
            $answer->save();

            $j = 0;
            foreach($kpis as $kpi)
            {
                DB::table('answer_kpi')->insert([
                    'answer_id' => $answer->id,
                    'kpi_id' => $kpi->id,  //system input
                    'effect' => $kpivalues[($j + $i * Kpi::count())],  //user input
                ]);
                $j++;
            }

            // for($j = 0; $j < Kpi::count(); $j++)
            // {
            //     DB::table('answer_kpi')->insert([
            //         'answer_id' => $answer->id,
            //         'kpi_id' => ($j + 1),  //system input
            //         'effect' => $kpivalues[($j + $i * Kpi::count())],  //user input
            //     ]);
            // }
        }

        return $this->index();
    }

    public function deleteQuestion()
    {
        $question_id = $_POST["question_id"];

        $question = Question::find($question_id);

        $question->delete();

        return $this->index();
    }

    public function showKpi($kpi_id)
    {
        $kpi = Kpi::find($kpi_id);

        return view('admin/kpi', compact('kpi'));
    }

    public function addKpi()
    {
        return view('admin/newkpi');
    }

    public function newKpi()
    {
        $kpi_name = $_POST["kpiname"];

        $kpi = new Kpi;
        $kpi->name = $kpi_name; //user input
        $kpi->save();

        $answers = Answer::all();

        foreach($answers as $answer)
        {
            DB::table('answer_kpi')->insert([
                'answer_id' => $answer->id,
                'kpi_id' => $kpi->id,
                'effect' => 1.00,
            ]);
        }
        

        return $this->index();
    }

    public function updateKpi()
    {
        $kpi_id = $_POST["kpi_id"];
        $kpi_name = $_POST["kpiname"];

        $kpi = Kpi::find($kpi_id);

        $kpi->name = $kpi_name;

        $kpi->save();

        return $this->index();
    }

    public function deleteKpi()
    {
        $kpi_id = $_POST["kpi_id"];

        $kpi = Kpi::find($kpi_id);

        $kpi->delete();

        return $this->index();
    }

    public function showNews($news_id)
    {
        $news = News::find($news_id);

        return view('admin/news', compact('news'));
    }

    public function addNews()
    {
        return view('admin/newNews');
    }

    public function newNews()
    {
        $newstitle = $_POST["newstitle"];
        $newstext = $_POST["newstext"];
        $newsquestion = $_POST["newsquestion"];

        $news = new News;
        $news->title = $newstitle; //user input
        $news->news = $newstext; //user input
        $news->question_id = $newsquestion; //user input
        $news->save();

        return $this->index();
    }

    public function updateNews()
    {
        $news_id = $_POST["news_id"];
        $newstitle = $_POST["newstitle"];
        $newstext = $_POST["newstext"];
        $newsquestion = $_POST["newsquestion"];

        $news = News::find($news_id);

        $news->title = $newstitle; //user input
        $news->news = $newstext; //user input
        $news->question_id = $newsquestion; //user input

        $news->save();

        return $this->index();
    }

    public function deleteNews()
    {
        $news_id = $_POST["news_id"];

        $news = News::find($news_id);

        $news->delete();

        return $this->index();
    }

    public function showFine($fine_id)
    {
        $fine = Fine::find($fine_id);

        return view('admin/fine', compact('fine'));
    }

    public function addFine()
    {
        return view('admin/newFine');
    }

    public function newFine()
    {
        $finetitle = $_POST["finetitle"];
        $finetext = $_POST["finetext"];
        $finequestion = $_POST["finequestion"];

        $fine = new Fine;
        $fine->title = $finetitle; //user input
        $fine->news = $finetext; //user input
        $fine->question_id = $finequestion; //user input
        $fine->save();

        return $this->index();
    }

    public function updateFine()
    {
        $fine_id = $_POST["fine_id"];
        $finetitle = $_POST["finetitle"];
        $finetext = $_POST["finetext"];
        $finequestion = $_POST["finequestion"];

        $fine = Fine::find($fine_id);

        $fine->title = $finetitle; //user input
        $fine->news = $finetext; //user input
        $fine->question_id = $finequestion; //user input

        $fine->save();

        return $this->index();
    }

    public function deleteFine()
    {
        $fine_id = $_POST["fine_id"];

        $fine = Fine::find($fine_id);

        $fine->delete();

        return $this->index();
    }
}
