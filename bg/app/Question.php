<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question', 'topic',
    ];

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function news()
    {
        return $this->hasOne('App\News');
    }

    public function fines()
    {
        return $this->hasOne('App\Fine');
    }
}
