<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'team_name', 'game_session_id',
    ];

    public function players()
    {
        return $this->hasMany('App\Player');
    }

    public function answers()
    {
        return $this->belongsToMany('App\Answer');
    }

    public function investments()
    {
        return $this->belongsToMany('App\Kpi');
    }
}
