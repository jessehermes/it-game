<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fine extends Model
{
    //
    protected $fillable = [
        'question_id', 'title', 'news',
    ];
}
