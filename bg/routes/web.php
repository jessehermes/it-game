<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Game join paths
Route::get('/', 'GameController@index');
Route::get('/join', 'GameController@index');
Route::get('/game/{teamid}', 'GameController@rejoin');
Route::get('/game/info/current-stage/{gameid}', 'GameController@getCurrentStage');
Route::get('/game/info/current-question-number/{gameid}', 'GameController@getCurrentQuestionNumber');
Route::get('/game/info/current-question/{gameid}', 'GameController@getQuestion');
Route::get('/game/info/news-update/{gameid}', 'GameController@getNews');
Route::get('/game/info/fine-update/{gameid}', 'GameController@getFine');

Route::post('/game/join', 'GameController@joinGame');
Route::post('/game/invest', 'GameController@investGame');
Route::post('/game/answer', 'GameController@answer');

//Kpi stuff
Route::get('/game/info/kpi/{teamid}', 'GameController@kpi');
Route::get('/game/info/financialkpi/{teamid}', 'GameController@financialKpi');
Route::get('/game/info/totalkpi/{teamid}', 'GameController@totalKpiScore');
// override reg
//Route::get('/register','Auth\RegisterController@showRegistrationForm')->name('register');

//Game host paths
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/host', 'HostController@index')->name('host');
Route::get('/game-session/{gameid}/next-question', 'HostController@nextQuestion');
Route::get('/game-session/{gameid}/show-question', 'HostController@showQuestion');
Route::get('/game-session/{gameid}/teams', 'HostController@getTeams');
Route::get('/game-session/{gameid}', 'HostController@rejoinSession');
Route::get('/game-session/{gameid}/allkpis', 'GameController@allTeamsKpi');
Route::get('/game-session/{gameid}/rapport', 'HostController@showRapport');
Route::get('/game-session/info/current-question/{gameid}', 'GameController@getQuestion');
Route::get('/game-session/info/total-answers/{gameid}', 'HostController@totalAnswers');

Route::post('/game-session', 'HostController@newSession');
Route::post('/game-session/rules', 'HostController@showRules');
Route::post('/game-session/invest', 'HostController@startInvest');
Route::post('/game-session/start', 'HostController@startSession');

Route::view('/graph', 'graph');

//Admin views
Route::get('/admin', 'AdminController@index');
Route::get('/admin/add-question/{numberofanswers}', 'AdminController@addQuestion');
Route::get('/admin/{question}', 'AdminController@showQuestion');
Route::get('/admin/kpi/add', 'AdminController@addKpi');
Route::get('/admin/kpi/{kpi}', 'AdminController@showkpi');
Route::get('/admin/news/add', 'AdminController@addNews');
Route::get('/admin/news/{news}', 'AdminController@showNews');
Route::get('/admin/fine/add', 'AdminController@addFine');
Route::get('/admin/fine/{fine}', 'AdminController@showFine');

Route::post('/admin/new-question', 'AdminController@newQuestion');
Route::post('/admin/update-question', 'AdminController@updateQuestion');
Route::post('/admin/delete-question', 'AdminController@deleteQuestion');
Route::post('/admin/new-kpi', 'AdminController@newKpi');
Route::post('/admin/update-kpi', 'AdminController@updateKpi');
Route::post('/admin/delete-kpi', 'AdminController@deleteKpi');
Route::post('/admin/new-news', 'AdminController@newNews');
Route::post('/admin/update-news', 'AdminController@updateNews');
Route::post('/admin/delete-news', 'AdminController@deleteNews');
Route::post('/admin/new-fine', 'AdminController@newFine');
Route::post('/admin/update-fine', 'AdminController@updateFine');
Route::post('/admin/delete-fine', 'AdminController@deleteFine');
