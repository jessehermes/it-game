@extends('layouts.app')

@section('scripts')

<script src="{{ asset('js/game.js') }}" defer></script>

<script>
    var game_session = {!! json_encode($game_session->toArray()) !!};
    var team_id = {!! $teamid !!};
    var question_answered = {!! $questionanswered !!};

    function newsShow() {
        document.getElementById("overlay").style.display = "block";
    }

    function newsHide() {
        document.getElementById("overlay").style.display = "none";
    }
    function fineShow() {
        document.getElementById("fine-overlay").style.display = "block";
    }

    function fineHide() {
        document.getElementById("fine-overlay").style.display = "none";
    }
</script>

@endsection

@section('content')

<div class="container">
    <p id="temp"></p> <!-- used when the game hasn't started yet -->
    <button style="float: right" onclick="newsShow()">Nieuws Artikelen</button>
    <div id="overlay" onclick="newsHide()">
        <p id="overlay-text"></p>
    </div>
    <div id="fine-overlay" onclick="fineHide()">
        <p id="fine-overlay-text"></p>
    </div>
    <div id="question-container">
        <p id="question"></p>

        <form id="answerForm" method="POST" action="/game/answer">
            @csrf

            <input id="questionid" type="hidden" name="question">
            <input id="gameid" type="hidden" name="gameid" value="{{ $game_session->id }}">
            <input id="teamid" type="hidden" name="teamid" value="{{ $teamid }}">

            <div class="card card-default">
                <div class="card-header">Antwoorden</div>

                <div id="answer-form" class="card-body">
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Kies Antwoord
                        </button>
                    </div>
                </div>  
            </div>

        </form>
    </div>

    <div id="kpi-container">
        <canvas id="myChart" width="400" height="200"></canvas>
        <canvas id="budgetChart" width="100" height="10"></canvas>
    </div>
</div>

@endsection
