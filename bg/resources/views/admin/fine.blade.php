@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">
                    <div style="width: 50%; display: inline-block;"><h3>Update fine</h3></div><div style="width: 50%; display: inline-block;">
                        <form method="POST" action="/admin/delete-fine">
                            @csrf
                            <input id="fine_id" type="number" name="fine_id" value="{{ $fine->id }}" hidden required>
                            <button style="float: right;" type="submit">
                                Delete fine
                            </button>
                        </form>
                    </div>    
                </div>

                <div class="card-body">
                    <form method="POST" action="/admin/update-fine">
                        @csrf

                        <input id="fine_id" type="number" name="fine_id" value="{{ $fine->id }}" hidden required>

                        <div class="form-group row">
                            <label for="finetitle" class="col-md-4 col-form-label text-md-right">fine title</label>

                            <div class="col-md-6">
                                <input id="finetitle" type="text" class="form-control" name="finetitle" value="{{ $fine->title }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="finetext" class="col-md-4 col-form-label text-md-right">fine text</label>

                            <div class="col-md-6">
                                <textarea id="finetext" class="resize-box" name="finetext" required>{{ $fine->news }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="finequestion" class="col-md-4 col-form-label text-md-right">fine question id</label>

                            <div class="col-md-6">
                                <input id="finequestion" type="text" class="form-control" name="finequestion" value="{{ $fine->question_id }}" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update fine
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
