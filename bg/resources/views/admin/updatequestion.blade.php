@extends('layouts.app')

@section('content')
<!-- <div class="container"> -->
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">
                    <div style="width: 50%; display: inline-block;"><h3>Update question</h3></div><div style="width: 50%; display: inline-block;">
                        <form method="POST" action="/admin/delete-question">
                            @csrf
                            <input id="question_id" type="number" name="question_id" value="{{ $question->id }}" hidden required>
                            <button style="float: right;" type="submit">
                                Delete question
                            </button>
                        </form>
                    </div>    
                </div>
                <div class="card-body">
                    <form method="POST" action="/admin/update-question">
                        @csrf

                        <div class="form-group row">
                            <label for="question" class="col-md-4 col-form-label text-md-right">Question</label>

                            <div class="col-md-6">
                                <input id="questionid" type="number" name="questionid" value="{{ $question->id }}" hidden required>
                                <input id="question" type="text" class="form-control" name="question" value="{{ $question->question }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="topic" class="col-md-4 col-form-label text-md-right">Onderdeel</label>

                            <div class="col-md-6">
                                <input id="topic" type="text" class="form-control" name="topic" value="{{ $question->topic }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                        <p>
                        <!-- @foreach ($answers as $answer)
                            @foreach ($answer->kpis as $kpi)
                                {{ $kpi->pivot->effect }}
                            @endforeach
                        @endforeach -->
                        </p>

                            <table style="width:100%">
                                <tr>
                                    <th>Answers</th>
                                    <th>cost</th>
                                    <th>revenue</th>
                                    <th>uitleg</th>
                                    @foreach ($kpis as $kpi)
                                        <th>{{ $kpi->name }}</th>
                                    @endforeach
                                </tr>
                                @foreach ($answers as $answer)
                                    <tr>
                                        <td><textarea class="resize-box" name="answers[]" required>{{ $answer->answer }}</textarea></td>
                                        <td><input type="number" class="form-control" name="costs[]" value="{{ $answer->cost }}" required></td>
                                        <td><input type="number" class="form-control" name="revenues[]" value="{{ $answer->revenue }}" required></td>
                                        <td><textarea class="resize-box" name="explanations[]">{{ $answer->explanation }}</textarea></td>
                                        @foreach ($answer->kpis as $kpi)
                                            <td>
                                                <select name="kpivalue[]">
                                                    <option value={{ $kpi->pivot->effect }}>{{ $kpi->pivot->effect }}</option>
                                                    <option value=1.08>++</option>
                                                    <option value=1.04>+</option>
                                                    <option value=1>1</option>
                                                    <option value=0.96>-</option>
                                                    <option value=0.92>--</option>
                                                </select>
                                            </td>
                                        @endforeach
                                        @if (count($answer->kpis) < count($kpis))
                                            @for ($i = 0; $i < (count($kpis) - count($answer->kpis)); $i++)
                                            <td>
                                                <select name="kpivalue[]">
                                                    <option value=1.08>++</option>
                                                    <option value=1.04>+</option>
                                                    <option value=1 selected="selected">1</option>
                                                    <option value=0.96>-</option>
                                                    <option value=0.92>--</option>
                                                </select>
                                            </td>
                                            @endfor
                                        @endif
                                    </tr>
                                @endforeach
                            </table>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update question
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- </div> -->
@endsection
