@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">
                    <div style="width: 50%; display: inline-block;"><h3>Update kpi</h3></div><div style="width: 50%; display: inline-block;">
                        <form method="POST" action="/admin/delete-kpi">
                            @csrf
                            <input id="kpi_id" type="number" name="kpi_id" value="{{ $kpi->id }}" hidden required>
                            <button style="float: right;" type="submit">
                                Delete kpi
                            </button>
                        </form>
                    </div>    
                </div>

                <div class="card-body">
                    <form method="POST" action="/admin/update-kpi">
                        @csrf

                        <input id="kpi_id" type="number" name="kpi_id" value="{{ $kpi->id }}" hidden required>

                        <div class="form-group row">
                            <label for="kpiname" class="col-md-4 col-form-label text-md-right">Kpi name</label>

                            <div class="col-md-6">
                                <input id="kpiname" type="text" class="form-control" name="kpiname" value="{{ $kpi->name }}" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update kpi
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
