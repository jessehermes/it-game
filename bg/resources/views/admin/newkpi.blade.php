@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form method="POST" action="/admin/new-kpi">
                @csrf

                <div class="form-group row">
                    <label for="kpiname" class="col-md-4 col-form-label text-md-right">Kpi name</label>

                    <div class="col-md-6">
                        <input id="kpiname" type="text" class="form-control" name="kpiname" required>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Add kpi
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
