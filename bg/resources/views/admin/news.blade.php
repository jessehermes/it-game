@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">
                    <div style="width: 50%; display: inline-block;"><h3>Update news</h3></div><div style="width: 50%; display: inline-block;">
                        <form method="POST" action="/admin/delete-news">
                            @csrf
                            <input id="news_id" type="number" name="news_id" value="{{ $news->id }}" hidden required>
                            <button style="float: right;" type="submit">
                                Delete news
                            </button>
                        </form>
                    </div>    
                </div>

                <div class="card-body">
                    <form method="POST" action="/admin/update-news">
                        @csrf

                        <input id="news_id" type="number" name="news_id" value="{{ $news->id }}" hidden required>

                        <div class="form-group row">
                            <label for="newstitle" class="col-md-4 col-form-label text-md-right">News title</label>

                            <div class="col-md-6">
                                <input id="newstitle" type="text" class="form-control" name="newstitle" value="{{ $news->title }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="newstext" class="col-md-4 col-form-label text-md-right">News text</label>

                            <div class="col-md-6">
                                <textarea id="newstext" class="resize-box" name="newstext" required>{{ $news->news }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="newsquestion" class="col-md-4 col-form-label text-md-right">News question id</label>

                            <div class="col-md-6">
                                <input id="newsquestion" type="text" class="form-control" name="newsquestion" value="{{ $news->question_id }}" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update news
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
