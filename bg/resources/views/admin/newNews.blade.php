@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form method="POST" action="/admin/new-news">
                @csrf

                <div class="form-group row">
                    <label for="newstitle" class="col-md-4 col-form-label text-md-right">News title</label>

                    <div class="col-md-6">
                        <input id="newstitle" type="text" class="form-control" name="newstitle" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="newstext" class="col-md-4 col-form-label text-md-right">News text</label>

                    <div class="col-md-6">
                        <textarea id="newstext" name="newstext" class="resize-box" required></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="newsquestion" class="col-md-4 col-form-label text-md-right">News question id</label>

                    <div class="col-md-6">
                        <input id="newsquestion" type="number" class="form-control" name="newsquestion" required>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Add news
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
