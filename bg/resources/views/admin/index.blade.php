@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <!-- <div class="col-md-8"> -->
            <div class="card card-default" style="width: 100%;">
                <div class="card-header">Overview of all questions - <a href="/admin/add-question/3">Add question</a></div>

                <div class="card-body">
                    <ol>
                        @foreach ($questions as $question)
                            <tr>
                                <li><a href="/admin/{{ $question->id }}">
                                    {{ $question->question }}
                                </a></li>
                            </tr>
                        @endforeach
                    </ol>
                </div>
            </div>
            
            <div class="card card-default" style="width: 100%;">
                <div class="card-header">Overview of all kpis - <a href="/admin/kpi/add">Add kpi</a></div>

                <div class="card-body">
                    <ol>
                        @foreach ($kpis as $kpi)
                            <tr>
                                <li><a href="/admin/kpi/{{ $kpi->id }}">
                                    {{ $kpi->name }}
                                </a></li>
                            </tr>
                        @endforeach
                    </ol>
                </div>
            </div>

            <div class="card card-default" style="width: 100%;">
                <div class="card-header">Overview of news articles - <a href="/admin/news/add">Add news</a></div>

                <div class="card-body">
                    <ol>
                        @foreach ($news as $new)
                            <tr>
                                <li><a href="/admin/news/{{ $new->id }}">
                                    {{ $new->title }}
                                </a></li>
                            </tr>
                        @endforeach
                    </ol>
                </div>
            </div>

            <div class="card card-default" style="width: 100%;">
                <div class="card-header">Overview of fines - <a href="/admin/fine/add">Add fine</a></div>

                <div class="card-body">
                    <ol>
                        @foreach ($fines as $fine)
                            <tr>
                                <li><a href="/admin/fine/{{ $fine->id }}">
                                    {{ $fine->title }}
                                </a></li>
                            </tr>
                        @endforeach
                    </ol>
                </div>
            </div>
        <!-- </div> -->
    </div>
</div>
@endsection
