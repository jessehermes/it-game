@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form method="POST" action="/admin/new-fine">
                @csrf

                <div class="form-group row">
                    <label for="finetitle" class="col-md-4 col-form-label text-md-right">Fine title</label>

                    <div class="col-md-6">
                        <input id="finetitle" type="text" class="form-control" name="finetitle" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="finetext" class="col-md-4 col-form-label text-md-right">Fine text</label>

                    <div class="col-md-6">
                        <textarea id="finetext" class="resize-box" name="finetext" required></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="finequestion" class="col-md-4 col-form-label text-md-right">Fine question id</label>

                    <div class="col-md-6">
                        <input id="finequestion" type="number" class="form-control" name="finequestion" required>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Add fine
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
