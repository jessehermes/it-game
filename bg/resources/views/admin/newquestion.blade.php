@extends('layouts.app')

@section('content')
<!-- <div class="container"> -->
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Create a new question</div>
                <div class="card-body">
                    <form method="POST" action="/admin/new-question">
                        @csrf

                        <div class="form-group row">
                            <label for="question" class="col-md-4 col-form-label text-md-right">Question</label>

                            <div class="col-md-6">
                                <input id="question" type="text" class="form-control" name="question" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="topic" class="col-md-4 col-form-label text-md-right">Onderdeel</label>

                            <div class="col-md-6">
                                <input id="topic" type="text" class="form-control" name="topic" required>
                            </div>
                        </div>

                        <div class="form-group row">

                            <table style="width:100%">
                                <tr>
                                    <th>Answers</th>
                                    <th>cost</th>
                                    <th>revenue</th>
                                    <th>uitleg</th>
                                    @foreach ($kpis as $kpi)
                                        <th>{{ $kpi->name }}</th>
                                    @endforeach
                                </tr>
                                @for ($i = 0; $i < $numberofanswers; $i++)
                                    <tr>
                                        <td><textarea class="resize-box" name="answers[]" required></textarea></td>
                                        <td><input type="number" class="form-control" name="costs[]" required></td>
                                        <td><input type="number" class="form-control" name="revenues[]" required></td>
                                        <td><textarea class="resize-box" name="explanations[]"></textarea></td>
                                        @foreach ($kpis as $kpi)
                                            <td>
                                                <select name="kpivalue[]">
                                                    <option value=1.08>++</option>
                                                    <option value=1.04>+</option>
                                                    <option value=1 selected="selected">1</option>
                                                    <option value=0.96>-</option>
                                                    <option value=0.92>--</option>
                                                </select>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endfor
                            </table>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Create question
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- </div> -->
@endsection
