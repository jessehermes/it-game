@extends('layouts.app')

@section('scripts')

<script>
    var gameid = {!! json_encode($gameid) !!};

    function checkTeams()
    {
        $.get("/game-session/" + gameid + "/teams").done(function( data ) {
            console.log(data);

            var teamList = document.getElementById("teamList");
            while (teamList.firstChild) {
                teamList.removeChild(teamList.firstChild);
            }

            for(var i = 0; i < data.length; i++) {
                console.log(data[i].name);
            }

            // Create the list element:
            var list = document.createElement('ul');

            for(var i = 0; i < data.length; i++) {
                // Create the list item:
                var item = document.createElement('li');

                // Set its contents:
                item.appendChild(document.createTextNode(data[i].name));

                // Add it to the list:
                list.appendChild(item);
            }

            teamList.appendChild(list);
        });
    }
</script>

@endsection

@section('content')
<div class="container">
    <div class="host-font">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">Game id: {{ $gameid }} <a style="float:right" href="/game-session/{{ $gameid }}/rapport">rapport</a></div>
                    <div class="card-body">
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button onclick="checkTeams()" class="btn btn-primary">Check Teams</button>
                            </div>
                        </div>
                        <h5>Teams</h5>
                        <div id="teamList"></div>
                        
                        <form method="POST" action="/game-session/rules">
                            @csrf
                            
                            <input id="gameid" type="hidden" name="gameid" value="{{ $gameid }}">

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Game Regels
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
