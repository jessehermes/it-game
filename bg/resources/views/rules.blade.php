@extends('layouts.app')

@section('scripts')

<script>
    var gameid = {!! json_encode($gameid) !!};
</script>

@endsection

@section('content')
<div class="container">
<div class="host-font">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div>
                <p id="rules">{!! $rulesText !!}</p>
                <img src="{{ URL::asset('img/hermes.png') }}">
            </div>
            <div class="card card-default">
                <div class="card-body">
                    
                    <form method="POST" action="/game-session/invest">
                        @csrf
                        
                        <input id="gameid" type="hidden" name="gameid" value="{{ $gameid }}">

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Invest
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection