@extends('layouts.app')

@section('scripts')

<script src="{{ asset('js/invest.js') }}" defer></script>

@endsection

@section('content')
<div class="container">
    <canvas id="myChart" width="400" height="200"></canvas>
    
</div>
@endsection
