@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Join a game</div>
                <div class="card-body">
                    <form method="POST" action="/game/join">
                        @csrf

                        <div class="form-group row">
                            <label for="game_id" class="col-md-4 col-form-label text-md-right">Game id:</label>

                            <div class="col-md-6">
                                <input id="game_id" type="number" class="form-control" name="game_id" required>
                            </div>

                            <label for="team_name" class="col-md-4 col-form-label text-md-right">Team naam:</label>

                            <div class="col-md-6">
                                <input id="team_name" type="text" class="form-control" name="team_name" required>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Join game
                                </button>
                            </div>
                        </div>
                        <p>{{ $error }}</p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
