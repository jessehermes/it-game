@extends('layouts.app')

@section('scripts')

<script src="{{ asset('js/invest.js') }}" defer></script>

<script>
    var game_session = {!! json_encode($game_session->toArray()) !!};
    var kpis = {!! json_encode($kpis->toArray()) !!};
    var invest_budget = {!! $investbudget !!};
</script>

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <p id="placeholder"></p> 
            <div id="investCard" class="card card-default">
                <div id="investHeader" class="card-header">Invest in kpi's</div>
                <div class="card-body">
                    <form id="investForm" method="POST" action="/game/invest">
                        @csrf
                        
                        <input id="gameid" type="hidden" name="gameid" value="{{ $game_session->id }}">
                        <input id="teamid" type="hidden" name="teamid" value="{{ $teamid }}">

                        <div class="slidecontainer" id="slidercontainer">
                        </div>

                        <div id="totalInvestment"></div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Invest
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
