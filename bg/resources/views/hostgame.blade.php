@extends('layouts.app')

@section('scripts')

<script src="{{ asset('js/host.js') }}" defer></script>


<script>
    var game_session = {!! json_encode($game_session->toArray()) !!};
</script>

@endsection

@section('content')
<!-- <div class="container"> -->
<div class="host-font">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div id="question-container" class="card card-default">
                <div id="current-question" class="card-header">Current Question text</div>
                <div id="answers" class="card-body">
                    
                </div>
            </div>
        </div>
    </div>

    <div class="form-group row mb-0">
        <div id="button-container" class="col-md-6 offset-md-4">
            <button id="next-button" class="btn btn-primary">show/next</button>
            <button id="totalkpi-button" class="btn btn-primary">Toon totaal score</button>
        </div>
        
        <div class="col-md-6 offset-md-4">
            <p id="total-answers">0 teams hebben geantwoord</p>
        </div>
    </div>

    <div style="text-align:center">
        <p id="winner-list"></p>
    </div>

    <div id="chart-container">
        <table id="kpi-chart-container" style="width:100%">

        </table>
    </div>
</div>
<!-- </div> -->
@endsection
