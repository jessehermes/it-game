@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Create a new game session</div>
                <div class="card-body">
                    <form method="POST" action="/game-session">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Number of Question</label>

                            <div class="col-md-6">
                                <input id="value" type="number" min="1" max="12" class="form-control" name="value" required>
                            </div>
                        </div>

                        <div class="card card-default">
                            <div class="card-header">All questions</div>

                            <div class="card-body">
                                @foreach ($questions as $question)
                                    <input type="checkbox" name="questions[]" value="{{ $question->id }}"><b>{{ $question->topic }}</b> - {{ $question->question }}<br>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Host game
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
