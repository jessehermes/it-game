$(document).ready(function () {

    navbarTitle = document.getElementById("appName");
    navbarTitle.innerHTML = game_session.name;

    updateKpi(game_session.id);
    updateQuestion(game_session.id);

    var refreshButton = document.createElement('button');
    refreshButton.innerHTML = 'Toon grafieken';
    refreshButton.classList.add("btn");
    refreshButton.classList.add("btn-primary");
    refreshButton.onclick = function () {
        updateKpi(game_session.id);
    };

    swapButton();

    totalkpiButton = document.getElementById("totalkpi-button");
    totalkpiButton.onclick = function () {
        showTotalKpi(game_session.id);
    };

    button_container = document.getElementById("button-container");
    button_container.appendChild(refreshButton);

    chartsUpdated = false;

    setInterval(function(){
        getTotalAnswers(game_session.id);
    }, 5000);
});

function swapButton()
{
    if (game_session.question_stage == 0)
    {
        nextButton = document.getElementById("next-button");
        nextButton.onclick = function () {
            showQuestion();
        };
        nextButton.innerHTML = "Toon vraag";
    }
    else
    {
        nextButton = document.getElementById("next-button");
        nextButton.onclick = function () {
            nextQuestion();
        };
        nextButton.innerHTML = "Volgende vraag";
    }
}

function nextQuestion() {
    $.get("/game-session/" + game_session.id + "/next-question").done(function (data) {
        game_session = data.game_session;
        updateQuestion(game_session.id);
        chartsUpdated = false;
        getTotalAnswers(game_session.id);
        swapButton();
    });
}

function showQuestion()
{
    $.get("/game-session/" + game_session.id + "/show-question").done(function (data) {
        game_session = data.game_session;
        swapButton();
    });
}

function getTotalAnswers(gameid)
{
    $.get("info/total-answers/" + gameid).done(function( data ) {
        text = document.getElementById("total-answers");
        if (data.game_finished != null)
        {
            if (data.game_finished)
            {
                text.innerHTML = "";
                return;
            }
        }
        text.innerHTML = data[0].number_of_answers + "/" + data[0].number_of_teams + " teams hebben geantwoord"

        if (data[0].number_of_answers == data[0].number_of_teams && !chartsUpdated)
        {
            updateKpi(game_session.id);
            chartsUpdated = true;
        }
    });
}

function updateQuestion(gameid)
{
    $.get("info/current-question/" + gameid).done(function( data ) {
        //Check if current question exists
        if (data.game_finished != null)
        {
            if (data.game_finished)
            {
                questionContainer = document.getElementById("question-container");
                questionContainer.style.display = "none";
            }
            return;
        }
        
        document.getElementById("current-question").innerHTML = data.question;

        var form = createAnswerForm(data.answers);
    });
}

function createAnswerForm(array) {
    
    var answerContainer = document.getElementById("answers");
    
    while (answerContainer.firstChild) {
        answerContainer.removeChild(answerContainer.firstChild);
    }
    
    alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q"];

    for (var i = 0; i < array.length; i++) {

        var el = document.createElement("p");
        el.name = "answer";
        el.value = array[i].id;

        var label = document.createElement("Label");

        label.htmlFor = i;
        label.innerHTML = alphabet[i] + ": Kost " + array[i].cost + ": " + array[i].answer;

        answerContainer.appendChild(el);
        answerContainer.appendChild(label);
        answerContainer.appendChild(document.createElement("BR"));
    }
}

function showTotalKpi(gameid) {

    questionContainer = document.getElementById("question-container");
    questionContainer.style.display = "none";
    container = document.getElementById("kpi-chart-container");
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }

    var colors = ["rgba(250, 30, 30, 0.8)", "rgba(30, 250, 30, 0.8)", "rgba(30, 30, 250, 0.8)", "rgba(200, 30, 200, 0.8)", "rgba(200, 200, 30, 0.8)", "rgba(30, 200, 200, 0.8)"];

    $.get(gameid + "/allkpis").done(function (data) {

        var dataset = [];
        var labels = [];
        var winnerScores = [];

        for (var j = 0; j < data.length; j++) // loop through each team
        {
            var values = [];

            for (var k = 0; k < data[j].totalkpis.length; k++) {
                values[k] = data[j].totalkpis[k].value;
                labels[k] = data[j].totalkpis[k].round;
            }

            color = j;
            while (color >= colors.length) {
                color -= colors.length;
            }
            dataset[j] = { "label": data[j].team_name, "data": values, "backgroundColor": colors[color], "borderColor": colors[color], "fill": false };
            winnerScores[j] = {"teamname": data[j].team_name, "finalscore": values[values.length-1]};
        }

        //sort array
        for (var j = 0; j < winnerScores.length - 1; j++)
        {
            for (var k = j + 1; k < winnerScores.length; k++)
            {
                if (winnerScores[j].finalscore < winnerScores[k].finalscore)
                {
                    temp = winnerScores[k];
                    winnerScores[k] = winnerScores[j];
                    winnerScores[j] = temp;
                }
            }
        }
        
        var winnerScoreText = "<ul style=\"list-style: none\">";
        for(var j = 0; j < winnerScores.length; j++)
        {
            winnerScoreText += "<li style=\"font-size: " + (30-j*3) + "px\"> #" + (j+1) + " " + winnerScores[j].teamname + " - " + winnerScores[j].finalscore + "</li>";
        }
        winnerScoreText += "</ul>";
        
        winnerText = document.getElementById("winner-list");
        winnerText.innerHTML = winnerScoreText;

        var canvas = document.createElement("canvas");
        canvas.width = 400;
        canvas.height = 200;

        container.appendChild(canvas);

        var ctx = canvas.getContext('2d');

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: dataset
            },
            options: {
                title: {
                    display: true,
                    text: "Totaal kpi score",
                    fontSize: 24
                },
                legend: {
                    labels: {
                        fontSize: 18
                    }
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'score',
                            fontSize: 18
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'ronde',
                            fontSize: 18
                        }
                        }]
                    } 
            }
        });

    });
}

function updateKpi(gameid) {

    winnerText = document.getElementById("winner-list");
    winnerText.innerHTML = "";
    questionContainer = document.getElementById("question-container");
    questionContainer.style.display = "block";
    container = document.getElementById("kpi-chart-container");
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }

    var colors = ["rgba(250, 30, 30, 0.8)", "rgba(30, 250, 30, 0.8)", "rgba(30, 30, 250, 0.8)", "rgba(200, 30, 200, 0.8)", "rgba(200, 200, 30, 0.8)", "rgba(30, 200, 200, 0.8)"];

    $.get(gameid + "/allkpis").done(function (data) {

        //Performance kpis
        for (var i = 0; i < data[0].kpis.length; i++) // loop through each kpi
        {
            var dataset = [];
            var labels = [];

            if (i % 2 == 0)
            {
                var tr = document.createElement('tr');
            }

            for (var j = 0; j < data.length; j++) // loop through each team
            {
                var values = [];

                for (var k = 0; k < data[j].kpis[i].values.length; k++) {
                    values[k] = data[j].kpis[i].values[k].value;
                    labels[k] = data[j].kpis[i].values[k].round;
                }

                color = j;
                while (color >= colors.length) {
                    color -= colors.length;
                }
                dataset[j] = { "label": data[j].team_name + " (" + data[j].team_id + ")", "data": values, "backgroundColor": colors[color], "borderColor": colors[color], "fill": false };
            }

            var canvas = document.createElement("canvas");
            // canvas.width = 400;
            // canvas.height = 200;

            
            var td = document.createElement('td');
            td.setAttribute('class', 'chartBox');

            td.appendChild(canvas);
            tr.appendChild(td);

            // container.appendChild(canvas);

            var ctx = canvas.getContext('2d');
            ctx.height = 100;
            ctx.width = 100;

            var myChart = new Chart(ctx, {
                type: 'line',
                backgroundColor: "#F5DEB3",
                data: {
                    labels: labels,
                    datasets: dataset,
                },
                options: {
                    title: {
                        display: true,
                        text: data[0].kpis[i].name,
                        fontSize: 24
                    },
                    legend: {
                        labels: {
                            // This more specific font property overrides the global property
                            fontSize: 18
                        }
                    },
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'score',
                                fontSize: 18
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'ronde',
                                fontSize: 18
                            }
                          }]
                      } 
                }
            });

            if (i % 2 == 1 || i == data[0].kpis.length-1)
            {
                container.appendChild(tr);
            }
        }

        //Financial kpis
        var dataset = [];
        var labels = [];

        for (var j = 0; j < data.length; j++) // loop through each team
        {
            var values = [];

            for (var k = 0; k < data[j].financialkpis.length; k++) {
                values[k] = data[j].financialkpis[k].ratio;
                labels[k] = k;
            }

            color = j;
            while (color >= colors.length) {
                color -= colors.length;
            }
            dataset[j] = { "label": data[j].team_name, "data": values, "backgroundColor": colors[color], "borderColor": colors[color], "fill": false };
        }

        var canvas = document.createElement("canvas");
        // canvas.width = 400;
        // canvas.height = 200;

        var td = document.createElement('td');
        var tr = document.createElement('tr');
        // td.setAttribute('class', 'chartBox');

        td.appendChild(canvas);
        tr.appendChild(td);

        container.appendChild(tr);

        // container.appendChild(canvas);

        var ctx = canvas.getContext('2d');

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: dataset
            },
            options: {
                title: {
                    display: true,
                    text: "Omzet/Kosten Ratio",
                    fontSize: 24
                },
                legend: {
                    labels: {
                        // This more specific font property overrides the global property
                        fontSize: 18
                    }
                },
                scales: {
                    yAxes: [{
                      scaleLabel: {
                        display: true,
                        labelString: 'ratio',
                        fontSize: 18
                      }
                    }],
                    xAxes: [{
                        scaleLabel: {
                          display: true,
                          labelString: 'ronde',
                          fontSize: 18
                        }
                      }]
                  } 
            }
        });
    });
}