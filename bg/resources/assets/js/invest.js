$(document).ready(function(){
    
    var x = document.getElementById("investCard");
    x.style.display = "none";
    document.getElementById("placeholder").innerHTML = "Wacht tot de game start.";

    var investHeader = document.getElementById("investHeader");
    investHeader.innerHTML = "Invest in kpi's - Total budget: " + invest_budget;

    navbarTitle = document.getElementById("appName");
    navbarTitle.innerHTML = game_session.name;

    createSliders();
    updateTotal();

    var investForm = document.getElementById("investForm");
    investForm.onsubmit = function validateInvestment(){

        if (sliderTotal() != invest_budget)
        {
            window.alert("Totale investering is niet " + invest_budget);
            return false;
        }
    
        return true;
    }

    getCurrentStage(game_session.id);

    setInterval(function(){
        getCurrentStage(game_session.id);
    }, 3000);
});

function getCurrentStage(gameid)
{
    $.get("info/current-stage/" + gameid).done(function( data ) {
        if(data > 0)
        {
            var x = document.getElementById("investCard");
            x.style.display = "block";
            document.getElementById("placeholder").innerHTML = "";
        }
    });
}

function updateTotal()
{
    var totalInvestment = document.getElementById("totalInvestment");
    while (totalInvestment.firstChild) {
        totalInvestment.removeChild(totalInvestment.firstChild);
    }
    totalInvestment.appendChild(document.createTextNode("Total investment is: " + sliderTotal()));
}

function sliderTotal()
{
    var sliderValues = document.getElementsByName("slider[]");
    var total = 0;

    for(i = 0; i < sliderValues.length; i++)
    {
        total += parseInt(sliderValues[i].value);
    }

    return total;
}

function createSliders()
{
    var sliderContainer = document.getElementById("slidercontainer");
    for(var i = 0; i < kpis.length; i++) {

        var sliderDiv = document.createElement("div");

        var text = document.createElement("p");
        text.appendChild(document.createTextNode(kpis[i].name));

        var slider = document.createElement("input");
        slider.type = "range";
        slider.id = "slider" + i;
        slider.name = "slider[]";
        slider.min = 0;
        slider.max = 50;
        slider.value = 0;
        slider.style = "width: 90%";

        var span = document.createElement("span");
        span.id = "sliderSpan" + i;
        span.appendChild(document.createTextNode(slider.value));

        slider.oninput = function()
        {
            idnumber = this.id.match(/\d+/)[0];
            var span2 = document.getElementById("sliderSpan" + idnumber);

            span2.innerHTML = this.value;

            updateTotal();
        }

        sliderDiv.appendChild(text);
        sliderDiv.appendChild(slider);
        sliderDiv.appendChild(span);
        sliderContainer.appendChild(sliderDiv);
    }
}

// function testGraph()
// {
//     var ctx = document.getElementById("myChart").getContext('2d');
//     var myChart = new Chart(ctx, {
//         type: 'bar',
//         data: {
//             labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
//             datasets: [{
//                 label: '# of Votes',
//                 data: [12, 19, 3, 5, 2, 3],
//                 backgroundColor: [
//                     'rgba(255, 99, 132, 0.2)',
//                     'rgba(54, 162, 235, 0.2)',
//                     'rgba(255, 206, 86, 0.2)',
//                     'rgba(75, 192, 192, 0.2)',
//                     'rgba(153, 102, 255, 0.2)',
//                     'rgba(255, 159, 64, 0.2)'
//                 ],
//                 borderColor: [
//                     'rgba(255, 99, 132, 1)',
//                     'rgba(54, 162, 235, 1)',
//                     'rgba(255, 206, 86, 1)',
//                     'rgba(75, 192, 192, 1)',
//                     'rgba(153, 102, 255, 1)',
//                     'rgba(255, 159, 64, 1)'
//                 ],
//                 borderWidth: 1
//             }]
//         },
//         options: {
//             scales: {
//                 yAxes: [{
//                     ticks: {
//                         beginAtZero:true
//                     }
//                 }]
//             }
//         }
//     });
// }

// function testGraph2()
// {
//     Chart.defaults.line.spanGaps = true;

//     var ctx = document.getElementById("myChart").getContext('2d');
//     var myChart = new Chart(ctx, {
//         type: 'line',
//         data: {
//           labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
//           datasets: [{
//             label: 'apples',
//             data: [12, 19, 3, 17, 6, 3, 7],
//             backgroundColor: "rgba(153, 255, 51, 0.4)"
//           }, {
//             label: 'oranges',
//             data: [2, 29, 5, 5, 2, 3, 10],
//             backgroundColor: "rgba(255, 153, 0, 0.4)"
//           }]
//         }
//       });
// }